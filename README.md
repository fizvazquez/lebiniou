# [Le Biniou](https://biniou.net)

[![pipeline status](https://gitlab.com/lebiniou/lebiniou/badges/master/pipeline.svg)](https://gitlab.com/lebiniou/lebiniou/commits/master)

# INPUT MIDI POC

Midi input to support. The fork idea is to have all the Combination Keys Actions allowed throw input Midi commands.

## Dependences

No new dependences are needed. This midi feature works with jackd2 midi libs. You only need to compile the software with jackaudio support. 

### Debian based distributions.

Before compile the software install this package:

  ```sh
  sudo apt-get install libjack-jackd2-dev
  ```
## Usage.

Once compiled the software execute it with the jackaudio plugin option:

```sh
  lebiniou -i jackaudio 
  ```

Lebiniou will create a new midi input device in the jack connection windows:

![](images/screenshot1.png) 

Connect your output midi device to the lebiniou input midi device. If you do not have a midi, you can use a virtual midi device. For example, you can use jack-keyboard package

  ```sh
  sudo apt-get install jack-keyboard
  ```

![](images/screenshot2.png) 

Once the Midi connected, you will see the midi input messages in your shell console

![](images/screenshot3.png) 

## Configure MIDI Input Lebiniou CC commands

In Orther to use this functionality you'll need to map your Midi output device CC with the lebiniou input actions. 

To do this, you will need to create a xml config file in your home directory:

  ```sh
  touch ~/.lebiniou/midi_cc.xml
  ```
Edit this file with something like this:

  ```sh
  <?xml version="1.0"?>
  <midi_input id="1558390164">
    <command_channels>
      <cc controller="32" value="1" subsystem="BT_CONTEXT" command="BC_NEXT" argument="BA_SEQUENCE"/>
      <cc controller="32" value="2" subsystem="BT_CONTEXT" command="BC_PREV" argument="BA_SEQUENCE"/>
    </command_channels>
  </midi_input>
  ```

In this example, we have related the MIDI CC 32 value 1 to the "Next Sequence" action. And MIDI CC 32 value 2 to de "Prev Sequence" action.

## FULL List Subsystem, Command, Argument available:

  ```sh
   BT_NONE,
   BT_IMAGEFADER,
   BT_CMAPFADER,
   BT_PLUGINS,
   BT_CONTEXT,
   BT_SEQMGR,
   BT_LAST,

   BC_NONE,
   BC_QUIT,
   BC_PREV,
   BC_NEXT,
   BC_RANDOM,
   BC_SWITCH,
   BC_RESET,
   BC_SELECT,
   BC_RELOAD,
   BC_SET,
   BC_INFO,
   BC_MOVE,
   BC_SAVE,
   BC_USE_BANKSET,
   BC_STORE_BANK,
   BC_USE_BANK,
   BC_CLEAR_BANK,
   BC_SAVE_BANKS,
   BC_SET_BANKMODE,
   BC_LAST,
   BC_LOCK,
   BC_VOLUME_SCALE,

   BA_NONE,
   BA_RANDOM,
   BA_SEQUENCE,
   BA_SCHEME,
   BA_COLORMAPS,
   BA_IMAGES,
   BA_FULLSCREEN,
   BA_ROTATIONS,
   BA_BOUNDARY,
   BA_OSD_CMAP,
   BA_CURSOR,
   BA_UP,
   BA_SAVE,
   BA_SCREENSHOT,
   BA_DOWN,
   BA_PREV,
   BA_NEXT,
   BA_LAYER_MODE,
   BA_SELECTED,
   BA_LENS,
   BA_BYPASS,
   BA_WEBCAM,
   BA_PULSE,
   BA_LAST,
   BA_DELAY,
   BA_SPAN,
   BA_MUTE,
   BA_SEQUENCE_FULL,
   BA_SEQUENCE_BARE,
   BA_OVERWRITE_FULL,
   BA_OVERWRITE_BARE,
  ```

## Use examples of commands:
 
  ```sh
Toggle 3D random rotations
BT_CONTEXT BC_RANDOM BA_ROTATIONS
Next 3D boundary
BT_CONTEXT BC_NEXT BA_BOUNDARY
Toggle 3D auto rotations
BT_CONTEXT BC_SWITCH BA_ROTATIONS 
Display current colormap
BT_CONTEXT BC_SWITCH BA_OSD_CMAP 

Toggle full-screen on/off
BT_CONTEXT BC_SWITCH BA_FULLSCREEN 
Show/hide mouse cursor
BT_CONTEXT BC_SWITCH BA_CURSOR 

Increase phase-space delay
BT_CONTEXT BC_NEXT BA_DELAY 
Decrease phase-space delay
BT_CONTEXT BC_PREV BA_DELAY 

Quit
BT_CONTEXT BC_QUIT BA_NONE 
Save the current sequence then exit
BT_CONTEXT BC_QUIT BA_SAVE 

Fill current frame with random pixels
BT_CONTEXT BC_RESET BA_RANDOM 
Clear the current frame
BT_CONTEXT BC_RESET BA_SEQUENCE 

Turn off all auto changes
BT_CONTEXT BC_RESET BA_NONE 
Toggle selected plugin on/off
BT_CONTEXT BC_SWITCH BA_SELECTED 

Use previous sequence
BT_CONTEXT BC_PREV BA_SEQUENCE 
Use next sequence
BT_CONTEXT BC_NEXT BA_SEQUENCE 

Take a screenshot
BT_CONTEXT BC_SAVE BA_SCREENSHOT 

Make a sequence from system schemes
BT_CONTEXT BC_RANDOM BA_SCHEME 
Select a random user sequence
BT_CONTEXT BC_RANDOM BA_SEQUENCE 
Next random mode
BC_NEXT BA_RANDOM 
Previous random mode
BC_PREV BA_RANDOM 

Auto colormaps on/off
BT_CONTEXT BC_SWITCH BA_COLORMAPS 
Auto images on/off
BT_CONTEXT BC_SWITCH BA_IMAGES 

Use the most recent sequence
BT_CONTEXT BC_RELOAD BA_SEQUENCE 

Bypass mode on/off
BT_CONTEXT BC_SWITCH BA_BYPASS 
Set webcam reference image
BT_CONTEXT BC_SET BA_WEBCAM 

Switch 3d cube on/off
BT_CONTEXT BC_SWITCH BA_BOUNDARY 
Toggle 3D world pulse on/off
BT_CONTEXT BC_SWITCH BA_PULSE 

Select next webcam
BT_CONTEXT BC_NEXT BA_WEBCAM 

Lock selected plugin
BT_CONTEXT BC_LOCK BA_SELECTED 

Increase spline span size
BT_CONTEXT BC_NEXT BA_SPAN 
Decrease spline span size
BT_CONTEXT BC_PREV BA_SPAN 

Freeze sound on/off
BT_CONTEXT BC_SWITCH BA_MUTE 

Scale volume up
BT_CONTEXT BC_VOLUME_SCALE BA_UP 
Scale volume down
BT_CONTEXT BC_VOLUME_SCALE BA_DOWN 

# --------------------------------------------------------------------
* Plugins
# --------------------------------------------------------------------
Select previous plugin
BT_PLUGINS BC_PREV BA_NONE 
Select next plugin
BT_PLUGINS BC_NEXT BA_NONE 

Scroll up in the plugins list
BT_PLUGINS BC_SELECT BA_UP 
Scroll down in the plugins list
BT_PLUGINS BC_SELECT BA_DOWN 

Reload selected plugin's .so
BT_PLUGINS BC_RELOAD BA_SELECTED 

# --------------------------------------------------------------------
* Sequence manager
# --------------------------------------------------------------------
Reset the current sequence
BT_SEQMGR BC_RESET BA_SEQUENCE 
Toggle selected plugin as a lens on/off
BT_SEQMGR BC_SWITCH BA_LENS 

Select previous plugin in the sequence
BT_SEQMGR BC_PREV BA_NONE 
Select next plugin in the sequence
BT_SEQMGR BC_NEXT BA_NONE 

Move selected plugin up in the sequence
BT_SEQMGR BC_MOVE BA_UP 
Move selected plugin down in the sequence
BT_SEQMGR BC_MOVE BA_DOWN 

Select default layer mode for the current plugin
BT_SEQMGR BC_RESET BA_LAYER_MODE 
Select previous layer mode
BT_SEQMGR BC_PREV BA_LAYER_MODE 
Select next layer mode
BT_SEQMGR BC_NEXT BA_LAYER_MODE 

Save current sequence as new (full)
BT_SEQMGR BC_SAVE BA_SEQUENCE_FULL 
Update current full sequence
BT_SEQMGR BC_SAVE BA_OVERWRITE_FULL 

Save current sequence as new (bare)
BT_SEQMGR BC_SAVE BA_SEQUENCE_BARE 
Update current bare sequence
BT_SEQMGR BC_SAVE BA_OVERWRITE_BARE 

# --------------------------------------------------------------------
* Colormaps
# --------------------------------------------------------------------
Select previous colormap
BT_CMAPFADER BC_SELECT BA_PREV 
Select next colormap
BT_CMAPFADER BC_SELECT BA_NEXT 
Select random colormap
BT_CMAPFADER BC_SELECT BA_RANDOM 
# --------------------------------------------------------------------
* Images
# --------------------------------------------------------------------
Select previous image
BT_IMAGEFADER BC_SELECT BA_PREV 
Select next image
BT_IMAGEFADER BC_SELECT BA_NEXT 
Select random image
BT_IMAGEFADER BC_SELECT BA_RANDOM 
# --------------------------------------------------------------------
* Banks
# --------------------------------------------------------------------
Set bank mode to sequences
BT_CONTEXT BC_SET_BANKMODE 0 
Set bank mode to colormaps
BT_CONTEXT BC_SET_BANKMODE 1 
Set bank mode to images
BT_CONTEXT BC_SET_BANKMODE 2 

Clear bank 1
BT_CONTEXT BC_CLEAR_BANK 0 
Clear bank 2
BT_CONTEXT BC_CLEAR_BANK 1 
Clear bank 3
BT_CONTEXT BC_CLEAR_BANK 2 
Clear bank 4
BT_CONTEXT BC_CLEAR_BANK 3 
Clear bank 5
BT_CONTEXT BC_CLEAR_BANK 4 
Clear bank 6
BT_CONTEXT BC_CLEAR_BANK 5 
Clear bank 7
BT_CONTEXT BC_CLEAR_BANK 6 
Clear bank 8
BT_CONTEXT BC_CLEAR_BANK 7 
Clear bank 9
BT_CONTEXT BC_CLEAR_BANK 8 
Clear bank 10
BT_CONTEXT BC_CLEAR_BANK 9 
Clear bank 11
BT_CONTEXT BC_CLEAR_BANK 10 
Clear bank 12
BT_CONTEXT BC_CLEAR_BANK 11 

Assign current sequence/colormap/image to bank 1
BT_CONTEXT BC_STORE_BANK 0 
Assign current sequence/colormap/image to bank 2
BT_CONTEXT BC_STORE_BANK 1
Assign current sequence/colormap/image to bank 3
BT_CONTEXT BC_STORE_BANK 2
Assign current sequence/colormap/image to bank 4
BT_CONTEXT BC_STORE_BANK 3
Assign current sequence/colormap/image to bank 5
BT_CONTEXT BC_STORE_BANK 4
Assign current sequence/colormap/image to bank 6
BT_CONTEXT BC_STORE_BANK 5
Assign current sequence/colormap/image to bank 7
BT_CONTEXT BC_STORE_BANK 6
Assign current sequence/colormap/image to bank 8
BT_CONTEXT BC_STORE_BANK 7
Assign current sequence/colormap/image to bank 9
BT_CONTEXT BC_STORE_BANK 8
Assign current sequence/colormap/image to bank 10
BT_CONTEXT BC_STORE_BANK 9
Assign current sequence/colormap/image to bank 11
BT_CONTEXT BC_STORE_BANK 10
Assign current sequence/colormap/image to bank 12
BT_CONTEXT BC_STORE_BANK 11

Use sequence/colormap/image in bank 1
BT_CONTEXT BC_USE_BANK 0 
Use sequence/colormap/image in bank 2
BT_CONTEXT BC_USE_BANK 1 
Use sequence/colormap/image in bank 3
BT_CONTEXT BC_USE_BANK 2 
Use sequence/colormap/image in bank 4
BT_CONTEXT BC_USE_BANK 3 
Use sequence/colormap/image in bank 5
BT_CONTEXT BC_USE_BANK 4 
Use sequence/colormap/image in bank 6
BT_CONTEXT BC_USE_BANK 5 
Use sequence/colormap/image in bank 7
BT_CONTEXT BC_USE_BANK 6 
Use sequence/colormap/image in bank 8
BT_CONTEXT BC_USE_BANK 7 
Use sequence/colormap/image in bank 9
BT_CONTEXT BC_USE_BANK 8 
Use sequence/colormap/image in bank 10
BT_CONTEXT BC_USE_BANK 9 
Use sequence/colormap/image in bank 11
BT_CONTEXT BC_USE_BANK 10
Use sequence/colormap/image in bank 12
BT_CONTEXT BC_USE_BANK 11 

Use bankset 1
BT_CONTEXT BC_USE_BANKSET 0 
Use bankset 2
BT_CONTEXT BC_USE_BANKSET 1 
Use bankset 3
BT_CONTEXT BC_USE_BANKSET 2 
Use bankset 4
BT_CONTEXT BC_USE_BANKSET 3 
Use bankset 5
BT_CONTEXT BC_USE_BANKSET 4 
Use bankset 6
BT_CONTEXT BC_USE_BANKSET 5 
Use bankset 7
BT_CONTEXT BC_USE_BANKSET 6 
Use bankset 8
BT_CONTEXT BC_USE_BANKSET 7 
Use bankset 9
BT_CONTEXT BC_USE_BANKSET 8 
Use bankset 10
BT_CONTEXT BC_USE_BANKSET 9 
Use bankset 11
BT_CONTEXT BC_USE_BANKSET 10
Use bankset 12
BT_CONTEXT BC_USE_BANKSET 11

Save the banks file
BT_CONTEXT BC_SAVE_BANKS BA_NONE 
  ```


Ask us in #biniou (freenode.net) for more information :)

# Installation

## GNU/Linux-based systems

  1. Install required dependencies

### Debian-based distributions

  ```sh
  sudo apt-get -qq update
  sudo apt-get -qq install autoconf pkg-config gcc make libglib2.0-dev libfftw3-dev libxml2-dev libfreetype6-dev libswscale-dev libsdl2-ttf-dev libcaca-dev libjack-dev pandoc libsndfile1-dev libmagickwand-dev
  ```

  2. Configure, compile and install

  The configure script has several [build options](BUILD.md).

  ```sh
  autoreconf -fi
  ./configure
  make
  sudo make install
  ```

  3. Follow the same steps for [lebiniou-data](https://gitlab.com/lebiniou/lebiniou-data) package, then

  4. Run

  ```sh
  lebiniou
  ```

  5. Get more options
  ```sh
  lebiniou --help
  man lebiniou
  ```

## BSD-based systems

If you want to build the [documentation](https://biniou.net/manual.html),
make sure you have the [pandoc](https://pandoc.org) package installed:
```sh
pandoc --version
pandoc 2.2
```

1. Fetch dependencies

* FreeBSD (12.0)
  ```sh
  pkg install autoconf automake pkgconf glib fftw3 libxml2 ffmpeg sdl2_ttf libcaca jackit ImageMagick
  ```

* NetBSD (8.0)
  ```sh
  pkg_add autoconf automake pkg-config glib2 fftw libxml2 ffmpeg4 SDL2_ttf libcaca jack ImageMagick
  ```

* OpenBSD (6.4)
  ```sh
  pkg_add glib2 fftw3 libxml ffmpeg sdl2-ttf libcaca jack ImageMagick
  ```

2. Configure, compile and install
  ```sh
  autoreconf -fi
  ./configure
  make
  make install
  ```


