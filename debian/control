Source: lebiniou
Section: graphics
Priority: optional
Maintainer: Olivier Girondel <olivier@biniou.info>
Build-Depends: debhelper (>= 11), libglib2.0-dev, libfftw3-dev, libxml2-dev (>= 2.6),
 libswscale-dev, libfreetype6-dev, libasound2-dev, libsndfile1-dev,
 libjack-dev | libjack-jackd2-dev, libsdl2-ttf-dev,
 libpulse-dev, libavutil-dev, libmagickwand-dev, pandoc
Standards-Version: 4.3.0
Vcs-Browser: https://gitlab.com/lebiniou/lebiniou
Vcs-Git: https://gitlab.com/lebiniou/lebiniou.git
Homepage: https://biniou.net

Package: lebiniou
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, fonts-freefont-ttf, lebiniou-data (>= 3.27)
Description: displays images that evolve with sound
 Le Biniou works with music, voice, ambient sounds, whatever acoustic
 source you choose.
 .
 When you run Le Biniou it gives an evolutionary rendering of the
 sound you are playing.
 .
 You are given two options to run Le Biniou: You can manage entirely
 the sequences and choose your own series of images from the default
 library, your colour scales, the kind of alteration you want to apply
 or you can let Le Biniou's artificial intelligence run on its own.
 .
 Forget the old visualizations you are familiar with, discover a new
 multidimensional – spatial and chromatic – way of comprehending music
 and sounds for either artistic, recreational or didactic purposes.
