/*
 *  Copyright 2014-2019 Frantz Balinski
 *  Copyright 2018-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * l'idée :
 *   diminuer la couleur du pixel jusqu'à zéro
 */

#include "context.h"


u_long id = 1410477171;
u_long options = BE_BLUR|BEQ_NORANDOM;
char desc[] = "Color fade-out effect";


static float decay;


void
run(Context_t *ctx)
{
  const Pixel_t *src;
  Pixel_t *dst;
  u_long i;
  Pixel_t col;

  src = active_buffer(ctx)->buffer;
  dst = passive_buffer(ctx)->buffer;

  for (i = 0; i < BUFFSIZE; i++) {
    col = *src++;

    if (col > PIXEL_MINVAL) {
      col = (Pixel_t) floorf(decay * col);
    } else {
      col = PIXEL_MINVAL;
    }

    *dst++ = col;
  }
}


void
on_switch_on(Context_t *ctx)
{
  decay = (float) pow(2, b_rand_double_range(log2(1), log2(31)));
  decay = decay / (decay + 1.f);
#ifdef DEBUG
  printf("[s] zblur1: col *= %.3f\n", decay);
#endif
}
