/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


u_long id = 1058309664;
u_long options = BE_SFX2D|BEQ_NORANDOM;
char desc[] = "Pulsing box";

#define BEAT_BAND 2

// FIXME check the log calculations part in input.c


void
create(Context_t *ctx)
{
  if (NULL == ctx->input) {
    options |= BEQ_DISABLED;
  }
}


void
run(Context_t *ctx)
{
  double max = ctx->input->spectrum_log[A_MONO][BEAT_BAND];
  u_short x1;

  if (max > 1.0) {
    max = 1.0;
  } else if (max < 0.0) {
    max = 0.0;
  }
  x1 = max * MAXX;

  draw_filled_box_nc(passive_buffer(ctx),
		     0, HHEIGHT + (HHEIGHT / 8),
		     x1, HHEIGHT - (HHEIGHT / 8), 255);
}
