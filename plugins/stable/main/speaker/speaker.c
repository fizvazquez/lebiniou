/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "../include/infinity.h"

u_long id = 1190050161;
u_long options = BE_DISPLACE;
char dname[] = "Speaker";
char desc[] = "Infinity effect which reacts to volume";


#define NB_FCT 10

static VectorField_t *vf = NULL;


static t_complex
fct(t_complex a, guint32 n, gint32 p1, gint32 p2)
{
  t_complex b;
  float fact;
  float an;
  float circle_size;
  float speed;
  float co, si;
  float nn = (float)n/9.0;

  a.x -= HWIDTH;
  a.y -= HHEIGHT;

  an = 0.015*(p1-2*nn)+0.002;
  co = cosf(an);
  si = sinf(an);

  circle_size = HEIGHT*nn*2;
  speed = (float)4000-p2*1000;

  b.x = (co*a.x-si*a.y);
  b.y = (si*a.x+co*a.y);

  fact = (sqrtf(b.x*b.x+b.y*b.y)-circle_size)/speed+1;
  b.x *= fact;
  b.y *= fact;

  b.x += HWIDTH;
  b.y += HHEIGHT;

  if (b.x < 0.0 ) {
    b.x = 0.0;
  } else if (b.x > (float)MAXX) {
    b.x = (float)MAXX;
  }
  if (b.y < 0.0) {
    b.y = 0.0;
  } else if (b.y > (float)MAXY) {
    b.y = (float)MAXY;
  }

  return b;
}


void
create(Context_t *ctx)
{
  if (ctx->input == NULL) {
    options |= BEQ_DISABLED;
  } else {
    vf = VectorField_new(NB_FCT, &fct);
  }
}


void
destroy(Context_t *ctx)
{
  if (vf != NULL) {
    VectorField_delete(vf);
  }
}


void
run(Context_t *ctx)
{
  u_char volume = volume = (u_char)(Input_get_volume(ctx->input) * 10);

  if (volume >= NB_FCT) {
    volume = NB_FCT-1;
  }

  VectorField_run(vf, ctx, volume);
}
