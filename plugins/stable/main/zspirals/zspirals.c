/*
 *  Copyright 1994-2019 Olivier Girondel
 *  Copyright 2014-2019 Frantz Balinski
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "brandom.h"
#include "context.h"
#include "translation.h"
#include "point2d.h"


u_long id = 1406046076;
u_long options = BE_DISPLACE;
char desc[] = "Poly-spirals effect";


static Translation_t *t_spiral = NULL;


#define NUMCENTERS  (16)

static Point2d_t  centers[NUMCENTERS];


#define DELTA_ANGLE  (M_PI_4)


static void init_params(void);
static Map_t cth_spiral(const u_short, const u_short);

static inline float
hypof(const float x, const float y)
{
  return sqrtf(x*x + y*y);
}


void
create(Context_t *ctx)
{
  t_spiral = Translation_new(&cth_spiral, &init_params);
}


void
destroy(Context_t *ctx)
{
  if (NULL != t_spiral) {
    Translation_delete(t_spiral);
  }
}


void
run(Context_t *ctx)
{
  Translation_run(t_spiral, ctx);
}


void
on_switch_on(Context_t *ctx)
{
  Translation_batch_init(t_spiral);
}


static void
init_params(void)
{
  int i;

  for (i = 0; i < NUMCENTERS; i++) {
    centers[i].x = b_rand_int_range(MINX, MAXX + 1);
    centers[i].y = b_rand_int_range(MINY, MAXY + 1);
  }
}


static Map_t
cth_spiral(const u_short xx, const u_short yy)
{
  Map_t m;
  float ang, cx, cy, dist, dx, dy, radius, x, y;
  short i;

  radius = hypof(WIDTH >> 2, HEIGHT >> 2);

  x = xx;
  y = yy;
  for (i = 0; i < NUMCENTERS; i++) {
    cx = centers[i].x;
    cy = centers[i].y;

    if ((y == MINY) || (y == MAXY)) {
      dx = ((cx - x) * 3) / 4;
      dy = cy - y;
    } else if ((x == MINX) || (x == MAXX)) {
      dx = cx - x;
      dy = ((cy - y) * 3) / 4;
    } else {
      dx = x - cx;
      dy = y - cy;
      dist = hypof(dx, dy);
      if (dist < radius) {
        ang = atan2f(dy, dx) - DELTA_ANGLE;
        dist /= 10;
        dx = -sinf(ang) * dist;
        dy =  cosf(ang) * dist;
      } else {
        dx = dy = 0;
      }
    }

    x += dx;
    y += dy;
  }

  m.map_x = abs((int) rintf(x)) % WIDTH;
  m.map_y = abs((int) rintf(y)) % HEIGHT;

  return m;
}
