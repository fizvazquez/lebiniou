/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "oscillo.h"


u_long id = 946482113;
u_long options = BE_SFX2D;
u_long mode = OVERLAY;
char desc[] = "Oscilloscope based on a sine-wave";


static Porteuse_t *P = NULL;

const float sin2_phi_inc = 0.0101667; /* do not ask me why --oliv3 */
const float sin2_freq_min = 2, sin2_freq_max = 10;

static float sin2_phi = 0;
static float sin2_freq;        /* initialized on create */
static float sin2_target_freq; /* initialized on create */
static float sin2_freq_inc;    /* initialized on create */
static int   connect = 1;


static inline float
rnd_freq()
{
  /* drand48() a l'arrache --oliv3 */
  return drand48() * (sin2_freq_max - sin2_freq_min) + sin2_freq_min;
}


static void
change_params()
{
  if (sin2_freq_inc > 0) {
    sin2_freq += sin2_freq_inc;
    if (sin2_freq > sin2_target_freq) {
      float new_freq = rnd_freq();
      while (new_freq >= sin2_freq) {
        new_freq = rnd_freq();
      }
      sin2_freq_inc = -(drand48() / 10 + .01);
      sin2_target_freq = new_freq;
    }
  } else {
    sin2_freq += sin2_freq_inc;
    if (sin2_freq < sin2_target_freq) {
      float new_freq = rnd_freq();
      while (new_freq <= sin2_freq) {
        new_freq = rnd_freq();
      }
      sin2_freq_inc = drand48() / 10 + .01;
      sin2_target_freq = new_freq;
    }
  }
  sin2_phi += sin2_phi_inc;
}


static void
init(Context_t *ctx)
{
  uint32_t i;
  Transform_t t;

  memset(&t, 0, sizeof(t));

  P->origin.x = 1;
  P->origin.y = HMAXY;

  t.v_j_factor = HMAXY / 2 * .85;

  for (i = 0; i < P->size; i++) {
    float y = (t.v_j_factor
               * sin (sin2_freq * (float)i /(float)(ctx->input->size - 1) + sin2_phi));

    t.v_before.x = 0;
    t.v_before.y = y;
    t.v_i.x = 1.0 / (float)(ctx->input->size - 1) * MAXX;
    t.v_i.y = 0;
    t.v_after.x = 0;
    t.v_after.y = -y;

    P->trans[i] = t;
  }

  Porteuse_init_alpha(P);
}


void
create(Context_t *ctx)
{
  if (ctx->input == NULL) {
    options |= BEQ_DISABLED;
  } else {
    P = Porteuse_new(ctx->input->size, A_MONO);
    sin2_freq = sin2_freq_min;
    sin2_target_freq = rnd_freq();
    sin2_freq_inc = drand48() / 10 + .01;
    init(ctx);
  }
}


void
destroy(Context_t *ctx)
{
  if (P != NULL) {
    Porteuse_delete(P);
  }
}


void
run(Context_t *ctx)
{
  Buffer8_clear(passive_buffer(ctx));
  Porteuse_draw(P, ctx, connect);
  change_params();
  init(ctx);
}
