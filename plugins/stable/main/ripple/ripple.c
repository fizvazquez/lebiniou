/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


u_long id = 957202985;
u_long options = BE_WARP|BE_LENS;
char desc[] = "Ripple effect";

/*
 * FIXME precompute the tables at startup
 * or when parameters will change --oliv3
 */


void
run(Context_t *ctx)
{
  short j, i;
  const float s = sqrtf((WIDTH * WIDTH) + (HEIGHT * HEIGHT));
  const float zoom_fact = 0.9;
  const float ripple_fact = 0.1;
  static u_short ripple_size = 8;
  static char dir = 1;
  u_short di = 0, dj = 0;

  Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  Buffer8_clear_border(src);

  for (j = -HHEIGHT; j < HHEIGHT; j++) {
    for (i = -HWIDTH; i < HWIDTH; i++)  {
      float dist = sqrtf((i*i) + (j*j)), sd;
      int si=0, sj=0;

      dist *= M_PI * ripple_size / s;
      sd = sinf(dist);

      si = (int)(di * (zoom_fact + (ripple_fact * sd)));
      sj = (int)(dj * (zoom_fact + (ripple_fact * sd)));

      if ((si < MINX) || (si > MAXX) || (sj < MINY) || (sj > MAXY)) {
        si = HWIDTH;
        sj = HHEIGHT;
      }

      set_pixel_nc(dst, di, dj, get_pixel_nc(src, si, sj));
      di++;
    }
    di = 0;
    dj++;
  }

  if (dir == 1) {
    if (++ripple_size == 49) {
      dir = -1;
    }
  } else { /* -1 */
    if (--ripple_size == 1) {
      dir = 1;
    }
  }
}
