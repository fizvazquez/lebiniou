/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "spline.h"
#include "particles.h"


u_long id = 1192734840;
u_long options = BE_SFX3D|BEQ_PARTICLES|BEQ_NORANDOM;
u_long mode = OVERLAY;
char desc[] = "Phase-space reconstruction with lines";

#define CONNECT   1

#if 0
#define MAX_PARTICLES PS_DEFAULT_MAX_PARTICLES
#else
#define MAX_PARTICLES PS_NOLIMIT
#endif

static Particle_System_t *ps = NULL;
static const Point3d_t ORIGIN = { { 0.0, 0.0, 0.0 } };


static void
Delay2_generate(Input_t *input)
{
  u_short i;

#define TTL 0.666
#define MUL 0.15
#define COL 255

  Point3d_t p;
  Particle_t *part = NULL;

  pthread_mutex_lock(&input->mutex);

  /* Map cubique (x y z) */
  p.pos.x = input->data[A_MONO][0];
  p.pos.y = input->data[A_MONO][1];
  p.pos.z = input->data[A_MONO][2];

  part = Particle_new_indexed(TTL, COL, p, p3d_mul(&p, MUL), ORIGIN, 0.0);
  Particle_System_add(ps, part);

  for (i = 1; i < input->size - 2; i++) {
    Point3d_t q;

    q.pos.x = p.pos.y;
    q.pos.y = p.pos.z;
    q.pos.z = input->data[A_MONO][i+2];

    p = q;

    part = Particle_new_indexed(TTL, COL, p, p3d_mul(&p, MUL), ORIGIN, 0.0);
    Particle_System_add(ps, part);
  }

  pthread_mutex_unlock(&input->mutex);
}


static void
Delay2_init_spline(Spline_t *s)
{
  /* init the spline from the particle system */
  GSList *p = NULL;
  u_long i = 0;

  for (p = ps->particles; p != NULL; p = g_slist_next(p), i++) {
    Particle_t *part = (Particle_t *)p->data;

    if (i > ps->nb_particles) {
      xerror("FUCK DAMN SHIT i= %li max= %li\n", i, s->nb_cpoints);
    }
    s->cpoints[i] = part->pos;
  }
}


static void
Delay2_draw_spline(Context_t *ctx, Spline_t *s)
{
  u_long i;
  Buffer8_t *dst = passive_buffer(ctx);
  const Params3d_t *params3d = &ctx->params3d;
  Input_t *input = ctx->input;

#if CONNECT
  /* with LINES */
  for (i = 0; i < s->nb_spoints-1; i++) {
    draw_line_3d(params3d, dst, &s->spoints[i], &s->spoints[i+1], Input_random_color(input));
  }
#else
  /* with DOTS */
  for (i = 0; i < s->nb_spoints; i++) {
    set_pixel_3d(params3d, dst, &s->spoints[i], Input_random_color(input));
  }
#endif
}


void
create(Context_t *ctx)
{
  if (ctx->input == NULL) {
    options |= BEQ_DISABLED;
  } else {
    ps = Particle_System_new(MAX_PARTICLES);
  }
}


void
destroy(Context_t *ctx)
{
  if (ps != NULL) {
    Particle_System_delete(ps);
  }
}


void
run(Context_t *ctx)
{
  if (Plugin_is_enabled(options)) {
    Buffer8_t *dst = passive_buffer(ctx);

    Buffer8_clear(dst);
    Particle_System_go(ps);
    Delay2_generate(ctx->input);

    if (ps->nb_particles) {
      Spline_t *spline = NULL;

      spline = Spline_new(SPAN_SIZE, ps->nb_particles);
      Delay2_init_spline(spline);
      Spline_compute(spline);
      Delay2_draw_spline(ctx, spline);
      Spline_delete(spline);
    }
  }
}
