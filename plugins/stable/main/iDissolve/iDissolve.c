/*
 *  Copyright 1994-2019 Olivier Girondel
 *  Copyright 2019 Tavasti
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

/* Idea:
   increment / decrement each pixel color value towards pixel value of picture */


#include "context.h"
#include "images.h"

u_long id = 1550933228;
u_long options = BE_GFX|BEQ_IMAGE;
u_long mode = NORMAL;
char desc[] = "Show images smoothly";


void
create(Context_t *ctx)
{
  if (images == NULL) {
    options |= BEQ_DISABLED;
  }
}


void
run(Context_t *ctx)
{
  Buffer8_t *dst = passive_buffer(ctx);
  u_long k;

  Buffer8_copy(active_buffer(ctx), dst);

  for (k = 0; k < BUFFSIZE; k++) {
    Pixel_t col = ctx->imgf->cur->buff->buffer[k];
    if (dst->buffer[k] > col) {
      dst->buffer[k]--;
    } else if (dst->buffer[k] < col) {
      dst->buffer[k]++;
    }
  }
}
