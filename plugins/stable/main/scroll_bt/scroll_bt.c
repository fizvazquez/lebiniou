/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


u_long id = 944684128;
u_long options = BE_SCROLL|BEQ_VER;
char dname[] = "Scroll up";
char desc[] = "Scroll the screen upwards";


void
run(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);
  Pixel_t save[WIDTH];

  memcpy((void *)&save, (const void *)src->buffer, WIDTH*sizeof(Pixel_t));
  memcpy((void *)(dst->buffer+WIDTH*sizeof(Pixel_t)), (const void *)src->buffer, (BUFFSIZE-WIDTH)*sizeof(Pixel_t));
  memcpy((void *)dst->buffer, (const void *)&save, WIDTH*sizeof(Pixel_t));
}
