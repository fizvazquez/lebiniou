/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


u_long id = 1546947361;
u_long options = BE_SFX3D;
u_long mode = OVERLAY;
char desc[] = "Phase-space reconstruction";


void
create(Context_t *ctx)
{
  if (NULL == ctx->input) {
    options |= BEQ_DISABLED;
  }
}


void
run(Context_t *ctx)
{
  Buffer8_t *dst = passive_buffer(ctx);
  const Params3d_t *params3d = &ctx->params3d;
  Input_t *input = ctx->input;
  const uint8_t delay = Context_get_phase_space_delay(ctx);
  uint32_t x = 0;
  uint32_t y = delay;
  uint32_t z = 2 * delay;

  Buffer8_clear(dst);

  pthread_mutex_lock(&input->mutex);
  for ( ; z < input->size; x++, y++, z++) {
    Point3d_t p;
    p.pos.x = input->data[A_MONO][x];
    p.pos.y = input->data[A_MONO][y];
    p.pos.z = input->data[A_MONO][z];
    Pixel_t color = Input_random_color(input);
    set_pixel_3d(params3d, dst, &p, color);
  }
  pthread_mutex_unlock(&input->mutex);
}
