/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


u_long id = 1103058151;
u_long options = BE_BLUR|BEQ_DIAG;
char dname[] = "D-Blur-1";
char desc[] = "Diagonal blur";


void
run(Context_t *ctx)
{
  u_short i, j;
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);

  Buffer8_init_mask_3x3(active_buffer(ctx));

  for (j = 1; j < MAXY - 1; j++)
    for (i = 1; i < MAXX - 1; i++) {
      u_short somme;
      /*
       * 2 0 1
       * 0 4 0   => 10
       * 1 0 2
       */
      somme =  get_pixel_nc(src, i - 1, j - 1) * 2;
      somme += get_pixel_nc(src, i + 1, j - 1);
      somme += get_pixel_nc(src, i, j    ) * 4;
      somme += get_pixel_nc(src, i - 1, j + 1);
      somme += get_pixel_nc(src, i + 1, j + 1) * 2;

      somme /= 10;

      set_pixel_nc(dst, i, j, (Pixel_t)(somme));
    }

  Buffer8_expand_border(dst);
}
