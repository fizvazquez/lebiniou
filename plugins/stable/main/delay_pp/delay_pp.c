/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "particles.h"


u_long id = 1547215841;
u_long options = BE_SFX3D|BEQ_PARTICLES;
u_long mode = OVERLAY;
char desc[] = "Phase-space reconstruction using particles";


static const Point3d_t ORIGIN = { { 0.0, 0.0, 0.0 } };
static Particle_System_t *ps = NULL;


static void
Delay1_particles(Context_t *ctx)
{
  Buffer8_t *dst = passive_buffer(ctx);
  const Params3d_t *params3d = &ctx->params3d;
  Input_t *input = ctx->input;
  const uint8_t delay = Context_get_phase_space_delay(ctx);
  uint32_t x = 0;
  uint32_t y = delay;
  uint32_t z = 2 * delay;

  Buffer8_clear(dst);

  // move particles
  Particle_System_go(ps);

  pthread_mutex_lock(&input->mutex);
  for ( ; (z < input->size) && Particle_System_can_add(ps); x++, y++, z++) {
    Point3d_t p;
    p.pos.x = input->data[A_MONO][x];
    p.pos.y = input->data[A_MONO][y];
    p.pos.z = input->data[A_MONO][z];
    const float ttl = Input_random_float_range(input, 0.2, 1);
    Pixel_t color = Input_random_color(input);
    const Particle_t *part = Particle_new_indexed(ttl, color, p3d_mul(&p, 2.5), p3d_mul(&p, .1), ORIGIN, 0.0);

    Particle_System_add(ps, part);
  }
  pthread_mutex_unlock(&input->mutex);

  Particle_System_draw(ps, params3d, dst);
}


void
create(Context_t *ctx)
{
  if (ctx->input == NULL) {
    options |= BEQ_DISABLED;
  } else {
    ps = Particle_System_new(PS_NOLIMIT);
  }
}


void
destroy(Context_t *ctx)
{
  if (ps != NULL) {
    Particle_System_delete(ps);
  }
}


void
run(Context_t *ctx)
{
  Delay1_particles(ctx);
}
