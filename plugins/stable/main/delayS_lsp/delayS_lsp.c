/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "spline.h"
#include "particles.h"
#include "../include/delay.h"

u_long id = 1547584147;
u_long options = BE_SFX3D|BEQ_PARTICLES;
u_long mode = OVERLAY;
char desc[] = "Stereo phase-space reconstruction with spline and particles";

static const Point3d_t ORIGIN = { { 0.0, 0.0, 0.0 } };
static Particle_System_t *ps = NULL;

static Spline_t *s[2] = { NULL, NULL };
static uint8_t delay = 0;
static uint8_t span_size = 0;


static void
DelayS_draw(Context_t *ctx)
{
  u_short i;
  Buffer8_t *dst = passive_buffer(ctx);
  const Params3d_t *params3d = &ctx->params3d;
  Input_t *input = ctx->input;
  u_long points;
  uint8_t c;

  Buffer8_clear(dst);

  for (c = 0; c < 2; c++) {
    points = s[c]->nb_spoints - 1;

    for (i = 0; i < points; i++) {
      Pixel_t color = Input_random_color(input);
      draw_line_3d(params3d, dst, &s[c]->spoints[i], &s[c]->spoints[i + 1], color);
    }
  }
}


static void
DelayS_particles(Context_t *ctx)
{
  u_short i;
  Input_t *input = ctx->input;
  Buffer8_t *dst = passive_buffer(ctx);
  uint8_t c;

  Particle_System_go(ps);

  for (c = 0; c < 2; c++) {
    for (i = 0; (i < s[c]->nb_spoints) && Particle_System_can_add(ps); i++) {
      const float ttl = Input_random_float_range(input, 1.5, 2.5);
      const Pixel_t col = Input_random_color(input);
      const Particle_t *part = Particle_new_indexed(ttl, col, s[c]->spoints[i], p3d_mul(&s[c]->spoints[i], .1), ORIGIN, 0.0);

      Particle_System_add(ps, part);
    }
  }

  Particle_System_draw(ps, &ctx->params3d, dst);
}


void
create(Context_t *ctx)
{
  if (ctx->input == NULL) {
    options |= BEQ_DISABLED;
  } else {
    ps = Particle_System_new(PS_NOLIMIT);
  }
}


static void
free_splines()
{
  if (s[0] != NULL) {
    Spline_delete(s[0]);
    Spline_delete(s[1]);
  }
}


void
destroy(Context_t *ctx)
{
  if (ps != NULL) {
    Particle_System_delete(ps);
  }
  free_splines();
}


void
run(Context_t *ctx)
{
  uint8_t current_delay = Context_get_phase_space_delay(ctx);
  uint8_t current_span_size = Context_get_span_size(ctx);

  if ((delay != current_delay) || (span_size != current_span_size)) {
    alloc_spline(ctx, &s[0], NULL, current_delay, NULL, current_span_size);
    alloc_spline(ctx, &s[1], &delay, current_delay, &span_size, current_span_size);
  }
  delay_spline(ctx, s[0], A_LEFT,  -DELAY_XOFFSET);
  delay_spline(ctx, s[1], A_RIGHT, +DELAY_XOFFSET);
  DelayS_draw(ctx);
  DelayS_particles(ctx);
}
