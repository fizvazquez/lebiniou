/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"

u_long id = 1089234657;
u_long options = BEQ_LAST;
char dname[] = "X-Recurrence";
u_long mode = OVERLAY;
char desc[] = "Cross-recurrence plot of the input";

/*
 * Please refer to the file rp.c for an introduction to recurrence plots
 */
#define DMAX 4.0

/*
 * Ok now, the Cross Recurrence Plot, taken from:
 * http://arxiv.org/pdf/physics/0201062 (.PDF)
 *
 * see also: http://www.agnld.uni-potsdam.de/~marwan/rp/crps.php
 */
static inline Pixel_t
get_color(const Input_t *input, const int i, const int j)
{
  float dist;

  /*
    static int last_i = -1, last_j = -1;
    static u_char last_color = 0;

    if ((i == last_i) && (j == last_j))
    return last_color;

    last_i = i;
    last_j = j;
  */

  /* get distance between the two vectors */
  float dx = input->data[A_LEFT][i+0]-input->data[A_RIGHT][j+0];
  float dy = input->data[A_LEFT][i+1]-input->data[A_RIGHT][j+1];
  float dz = input->data[A_LEFT][i+2]-input->data[A_RIGHT][j+2];
  float dt = input->data[A_LEFT][i+3]-input->data[A_RIGHT][j+3];

  dx *= dx;
  dy *= dy;
  dz *= dz;
  dt *= dt;

  dist = sqrtf(dx + dy + dz + dt);
  dist /= DMAX;

  /* 	return (last_color = (255 - (u_char)(255 * dist))); */
  return (255 - (Pixel_t)(255 * dist));
}


void
create(Context_t *ctx)
{
  if (ctx->input == NULL) {
    options |= BEQ_DISABLED;
  }
}


void
run(Context_t *ctx)
{
  int ii, jj;
  Buffer8_t *dst = passive_buffer(ctx);

  Buffer8_clear(dst);

  pthread_mutex_lock(&ctx->input->mutex);

  for (jj = 0; jj < MINSCREEN; jj++) {
    int j = (int)((float)jj
		  / (float)MINSCREEN
		  * (float)(ctx->input->size-3.0));
    int last_i = -1;
    Pixel_t last_c = 0;

    for (ii = 0; ii < MINSCREEN; ii++) {
      Pixel_t c = last_c;
      int i = (int)((float)ii
		    / (float)MINSCREEN
		    * (float)(ctx->input->size-3.0));

      if (i != last_i) {
	c = get_color(ctx->input, i, j);
	last_i = i;
	last_c = c;
      }

      /* not-optimized plot */
      set_pixel_nc(dst, CENTERX-HMINSCREEN+ii, jj, c);
    }
  }

  pthread_mutex_unlock(&ctx->input->mutex);
}
