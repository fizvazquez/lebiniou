/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"

u_long id = 1073678364;
u_long options = BE_SFX3D|BEQ_NORANDOM;
char dname[] = "OF A-Spiral";
u_long mode = OVERLAY;
char desc[] = "3D Archimedean spiral";


void
create(Context_t *ctx)
{
  if (ctx->input == NULL) {
    options |= BEQ_DISABLED;
  }
}


void
run(Context_t *ctx)
{
  static float t  = 0;
  Buffer8_t *dst = passive_buffer(ctx);

  Buffer8_clear(dst);

  float random = Input_get_volume(ctx->input);

  for (t = 0 ; t < 8 * M_PI ; t += 0.1) {
    Point3d_t P;

    P.pos.x = random * expf(0.15 * t) * cosf(2 * t);
    P.pos.y = random * expf(0.15 * t) * sinf(2 * t);
    P.pos.z = -1 + random * expf(0.15 * t);

    set_pixel_3d(&ctx->params3d, dst, &P, Input_random_color(ctx->input));
  }
}
