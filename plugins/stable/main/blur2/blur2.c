/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


u_long id = 944355800;
u_long options = BE_BLUR;
char desc[] = "Blur filter";


void
run(Context_t *ctx)
{
  const Pixel_t *n, *s, *w, *c, *e;
  Pixel_t *d;
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);
  u_long i;

  Buffer8_init_mask_3x3(active_buffer(ctx));

  n = src->buffer + 1;
  s = src->buffer + 2 * WIDTH + 1;
  w = src->buffer + WIDTH;
  c = src->buffer + (WIDTH + 1);
  e = src->buffer + (WIDTH + 2);

  d = dst->buffer + (WIDTH + 1);
  for (i = BUFFSIZE - WIDTH - (WIDTH + 1); i--; ) {
    short somme =
      *n++
      + (*w++ << 1) + (*c++ << 1) + (*e++ << 1)
      + *s++;
    somme >>= 3;
    somme &= 255;
    *d++ = (Pixel_t)(somme);
  }

  Buffer8_expand_border(dst);
}
