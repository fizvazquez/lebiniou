/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "images.h"


u_long id = 1090876849;
u_long options = BE_GFX|BEQ_IMAGE;
u_long mode = NORMAL;
char desc[] = "Random image squares";


#define SQUARE_SIZE 40
#define SQUARES_PER_TURN  10


void
create(Context_t *ctx)
{
  if (images == NULL) {
    options |= BEQ_DISABLED;
  }
}


void
run(Context_t *ctx)
{
  int s;

  Buffer8_t *dst = passive_buffer(ctx);

  Buffer8_copy(active_buffer(ctx), dst);

  for (s = 0; s < SQUARES_PER_TURN; s++) {
    int i, j;

    u_short sx = b_rand_int_range(0, MAXX-SQUARE_SIZE);
    u_short sy = b_rand_int_range(0, MAXY-SQUARE_SIZE);

    for (j = 0; j < SQUARE_SIZE; j++)
      for (i = 0; i < SQUARE_SIZE; i++)
	set_pixel_nc(dst,
		     sx+i, sy+j,
		     get_pixel_nc(ctx->imgf->cur->buff, sx+i, sy+j));
  }
}
