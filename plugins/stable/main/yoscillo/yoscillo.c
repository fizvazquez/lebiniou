/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "oscillo.h"


u_long id = 946482111;
u_long options = BE_SFX2D;
char dname[] = "Y oscillo";
u_long mode = OVERLAY;
char desc[] = "Vertical mono oscilloscope";

static Porteuse_t *P = NULL;
static int connect = 1;


static void
init()
{
  uint32_t i;
  Transform_t t;

  memset(&t, 0, sizeof(t));

  P->origin.x = CENTERX;
  P->origin.y = 0;

  t.v_j_factor = HMAXX * 0.85;
  t.v_i.y = 1.0 / (float)(P->size - 1) * (float)MAXY;

  for (i = 0; i < P->size; i++) {
    P->trans[i] = t;
  }

  Porteuse_init_alpha(P);
}


void
create(Context_t *ctx)
{
  if (ctx->input != NULL) {
    P = Porteuse_new(ctx->input->size, A_MONO);
    init();
  } else {
    options |= BEQ_DISABLED;
  }
}


void
destroy(Context_t *ctx)
{
  if (P != NULL) {
    Porteuse_delete(P);
  }
}


void
run(Context_t *ctx)
{
  Buffer8_clear(passive_buffer(ctx));
  Porteuse_draw(P, ctx, connect);
}
