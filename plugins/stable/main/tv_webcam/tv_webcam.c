/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"

uint32_t id = 1172430309;
u_long options = BE_GFX|BEQ_WEBCAM|BEQ_MUTE_CAM;
char desc[] = "Webcam plugin";
uint8_t mode = OVERLAY;


void
create(Context_t *ctx)
{
  if (!ctx->webcams) {
    options |= BEQ_DISABLED;
  }
}


void
run(Context_t *ctx)
{
  pthread_mutex_lock(&ctx->cam_mtx[ctx->cam]);
  Buffer8_copy(ctx->cam_save[ctx->cam][0], passive_buffer(ctx));
  pthread_mutex_unlock(&ctx->cam_mtx[ctx->cam]);
}
