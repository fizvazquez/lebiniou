/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "translation.h"


u_long id = 949050897;
u_long options = BE_DISPLACE;
char dname[] = "Spiral";
char desc[] = "Spiral effect";

/* Pour garder le centre de la translation bien visible dans le screen */
#define SHIFT_X (WIDTH / 10)
#define SHIFT_Y (HEIGHT / 10)


static Translation_t *t_spiral = NULL;

static int cx, cy;
const float q = M_PI / 2;
const float p = 45.0 / 180.0 * M_PI;


static Map_t
cth_spiral(const u_short i, const u_short j)
{
  int dx,dy;
  Map_t m;

  if ((j == MINY) || (j == MAXY)) {
    dx = (float)(cx - i) * 0.75;
    dy = cy - j;
  } else {
    int dist;
    float ang;

    dist = sqrt((i-cx)*(i-cx) + (j-cy)*(j-cy));

    if (i==cx) {
      if (j>cx) {
        ang = q;
      } else {
        ang = -q;
      }
    } else {
      ang = atan((float)(j-cy)/(i-cx));
    }

    if (i<cx) {
      ang += M_PI;
    }

    dx = ceil(-sin(ang-p)*dist/10.0);
    dy = ceil(cos(ang-p)*dist/10.0);

    if ((i == MINX) || (i == MAXX)) {
      dx = cx - i;
      dy = (float)(cy - j) * 0.75;
    }
  }

  m.map_x = abs((i+dx) % WIDTH);
  m.map_y = abs((j+dy) % HEIGHT);

  return m;
}


static void
init_params()
{
  cx = b_rand_int_range(SHIFT_X, (MAXX - SHIFT_X));
  cy = b_rand_int_range(SHIFT_Y, (MAXY - SHIFT_Y));
}


void
on_switch_on(Context_t *ctx)
{
  Translation_batch_init(t_spiral);
}


void
create(Context_t *ctx)
{
  t_spiral = Translation_new(&cth_spiral, &init_params);
}


void
destroy(Context_t *ctx)
{
  Translation_delete(t_spiral);
}


void
run(Context_t *ctx)
{
  Translation_run(t_spiral, ctx);
}
