/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "oscillo.h"


u_long id = 946482114;
u_long options = BE_SFX2D;
u_long mode = OVERLAY;
char desc[] = "Draw an oscillo in a radar way";


static Porteuse_t *P = NULL;

static Point2d_t last_polar;
static float     polar_theta, polar_inc_theta;
static u_short   polar_radius;
static float     polar_length;
static int       connect = 1;


void
init(const Input_t *input)
{
  uint32_t i;
  Transform_t t;

  memset(&t, 0, sizeof(t));

  P->origin = last_polar;

  t.v_j_factor = MAXY/4 * 0.85;

  polar_inc_theta = 2. * M_PI * polar_length / (float)(input->size);

  for (i = 0; i < P->size; i++) {
    Point2d_t next, dsignal;

    next.x = CENTERX + polar_radius * cos(polar_theta);
    next.y = CENTERY + polar_radius * sin(polar_theta);

    dsignal = p2d_sub(&next, &last_polar);
    t.v_i = dsignal;
    last_polar = p2d_add(&last_polar, &t.v_i);

    P->trans[i] = t;
    polar_theta += polar_inc_theta;
  }
  polar_theta -= 2.0 * M_PI * (int)(polar_theta / (2.0 * M_PI));

  Porteuse_init_alpha(P);
}


void
create(Context_t *ctx)
{
  if (ctx->input == NULL) {
    options |= BEQ_DISABLED;
  } else {
    P = Porteuse_new(ctx->input->size, A_MONO);

    polar_theta = 0.0;
    polar_inc_theta = 0.01;
    polar_length = 0.666;
    polar_radius = HMAXY*2.0/3.0;

    last_polar.x = CENTERX + polar_radius * cos (polar_theta);
    last_polar.y = CENTERY + polar_radius * sin (polar_theta);

    polar_theta += polar_inc_theta;

    init(ctx->input);
  }
}


void
destroy(Context_t *ctx)
{
  if (P != NULL) {
    Porteuse_delete(P);
  }
}


void
run(Context_t *ctx)
{
  Buffer8_clear(passive_buffer(ctx));
  Porteuse_draw(P, ctx, connect);
  init(ctx->input);
}
