/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "oscillo.h"


u_long id = 946482109;
u_long options = BE_SFX2D;
u_long mode = OVERLAY;
char desc[] = "Rotating oscilloscope";


static Porteuse_t *P = NULL;

/* FIXME hardcoded */
#define RADIUS (HMINSCREEN * 0.85)

static float alpha = 0.0;
static int   connect = 1;


static void
init()
{
  uint32_t i;
  Transform_t t;

  memset(&t, 0, sizeof(t));

  P->origin.x = CENTERX - RADIUS * cos (alpha);
  P->origin.y = CENTERY - RADIUS * sin (alpha);

  t.v_j_factor = HMAXY * 0.85;
  t.v_i.x = (2.0 / (float)(P->size - 1) * (float)RADIUS) * cos (alpha);
  t.v_i.y = (2.0 / (float)(P->size - 1) * (float)RADIUS) * sin (alpha);

  for (i = 0; i < P->size; i++) {
    P->trans[i] = t;
  }

  Porteuse_init_alpha (P);
}


void
create(Context_t *ctx)
{
  if (ctx->input == NULL) {
    options |= BEQ_DISABLED;
  } else {
    P = Porteuse_new(ctx->input->size, A_MONO);
    init ();
  }
}


void
destroy(Context_t *ctx)
{
  if (P != NULL) {
    Porteuse_delete (P);
  }
}


void
on_switch_on(Context_t *ctx)
{
  /* connect = b_rand_boolean(); */
}


void
run(Context_t *ctx)
{
  Buffer8_clear(passive_buffer(ctx));
  Porteuse_draw(P, ctx, connect);

  /* FIXME hardcoded */
  alpha += 0.005;

  if (alpha > 2*M_PI) {
    alpha -= 2*M_PI;
  }
  init();
}
