/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "shuffler.h"


/*
 * same as cirrus.c, but adds a "reverse" parameter.
 * this may lead to flashy colormap updates, which are
 * a little more aggressive than in Cirrus
 */

u_long id = 1181600027;
u_long options = BEQ_COLORMAP;
char dname[] = "Cirrus 2";
u_long mode = NONE;
char desc[] = "Neon color";


#define NCOLORS 7
static rgba_t colors[NCOLORS] = {
  { { 255, 0, 0, 0 } },
  { { 0, 255, 0, 0 } },
  { { 0, 0, 255, 0 } },
  { { 255, 255, 0, 0 } },
  { { 255, 0, 255, 0 } },
  { { 0, 255, 255, 0 } },
  { { 255, 255, 255, 0 } }
};


static u_char src_color_idx, dst_color_idx;
static rgba_t dst_color;
static int reverse;

static Alarm_t *cirrus_alarm = NULL;
static Shuffler_t *shuffler = NULL;


void
create(Context_t *ctx)
{
  cirrus_alarm = Alarm_new(3, 10);
  shuffler = Shuffler_new(NCOLORS);
}


void
destroy(Context_t *ctx)
{
  Alarm_delete(cirrus_alarm);
  Shuffler_delete(shuffler);
}


static void
set_colors()
{
  dst_color = colors[dst_color_idx];
}


static void
random_color()
{
  dst_color_idx = Shuffler_get(shuffler);
  set_colors();

  Alarm_init(cirrus_alarm);
  reverse = b_rand_boolean();
}


void
on_switch_on(Context_t *ctx)
{
  src_color_idx = Shuffler_get(shuffler);
  random_color();
}


void
on_switch_off(Context_t *ctx)
{
  CmapFader_set(ctx->cf);
}


#define MIX(SRC, DST, IDX, I, PCT) ((SRC.rgbav[IDX]*(1.0-PCT) + DST.rgbav[IDX]*PCT*(reverse ? 255.0-I : I)/255.0))

void
run(Context_t *ctx)
{
  u_short i;
  rgba_t *col;

  /* Turn off auto colormaps */

  /* NOTE: Ideally, we would do this in on_switch_on, but the current engine
   * calls Context_randomize _after_ the plugins are switched on.
   * So we do this here: */
  ctx->sm->cur->auto_colormaps = ctx->cf->on = 0;

  /* Now, on to the real job */
  for (i = 0; i < 256; i++) {
    float pct;

    col = &(ctx->cf->cur)->colors[i];
    pct = Alarm_elapsed_pct(cirrus_alarm);

    col->col.r = MIX(ctx->cf->cur->colors[i], dst_color, 0, i, pct);
    col->col.g = MIX(ctx->cf->cur->colors[i], dst_color, 1, i, pct);
    col->col.b = MIX(ctx->cf->cur->colors[i], dst_color, 2, i, pct);
  }

  /* Ask the output driver to update it's colormap */
  ctx->cf->refresh = 1;

  /* Change to next color */
  if (Alarm_ring(cirrus_alarm)) {
    src_color_idx = dst_color_idx;
    random_color();
  }
}
