/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "spline.h"
#include "particles.h"
#include "../include/delay.h"

// #define PARTICLES

u_long id = 1547466099;
u_long options = BE_SFX3D
#ifdef PARTICLES
                 |BEQ_PARTICLES
#endif
                 ;
u_long mode = OVERLAY;
char desc[] = "Stereo phase-space reconstruction with spline"
#ifdef PARTICLES
              " and particles"
#endif
              ;

// to separate left and right audio channels
#define DELTA 0.5

#ifdef PARTICLES
static const Point3d_t ORIGIN = { { 0.0, 0.0, 0.0 } };
static Particle_System_t *ps = NULL;
#endif
static Spline_t *s[2] = { NULL, NULL };
static uint8_t delay = 0;
static uint8_t span_size = 0;


static void
DelayS_draw(Context_t *ctx)
{
  u_short i;
  Buffer8_t *dst = passive_buffer(ctx);
  const Params3d_t *params3d = &ctx->params3d;
  Input_t *input = ctx->input;
  u_long points;
  uint8_t c;

  Buffer8_clear(dst);

  for (c = 0; c < 2; c++) {
    points = s[c]->nb_spoints - 1;

    for (i = 0; i < points; i++) {
      Pixel_t color = Input_random_color(input);
      draw_line_3d(params3d, dst, &s[c]->spoints[i], &s[c]->spoints[i + 1], color);
    }
  }
}


#ifdef PARTICLES
static void
DelayS_particles(Context_t *ctx)
{
  u_short i;
  Input_t *input = ctx->input;
  Buffer8_t *dst = passive_buffer(ctx);

  Particle_System_go(ps);

  for (i = 0; (i < s->nb_spoints) && Particle_System_can_add(ps); i++) {
    Particle_t *p = NULL;
    float ttl = Input_random_float_range(input, 0.8, 2.0);
    Pixel_t col = Input_random_color(input);
    p = Particle_new_indexed(ttl, col, s->spoints[i], p3d_mul(&s->spoints[i], 0.25), ORIGIN, 0.0);

    Particle_System_add(ps, p);
  }

  Particle_System_draw(ps, &ctx->params3d, dst);
}
#endif


void
create(Context_t *ctx)
{
  if (ctx->input == NULL) {
    options |= BEQ_DISABLED;
  } else {
#ifdef PARTICLES
    ps = Particle_System_new(PS_NOLIMIT);
#endif
  }
}


static void
free_splines()
{
  if (s[0] != NULL) {
    Spline_delete(s[0]);
    Spline_delete(s[1]);
  }
}


void
destroy(Context_t *ctx)
{
#ifdef PARTICLES
  if (ps != NULL) {
    Particle_System_delete(ps);
  }
#endif
  free_splines();
}


void
run(Context_t *ctx)
{
  uint8_t current_delay = Context_get_phase_space_delay(ctx);
  uint8_t current_span_size = Context_get_span_size(ctx);

  if ((delay != current_delay) || (span_size != current_span_size)) {
    alloc_spline(ctx, &s[0], NULL, current_delay, NULL, current_span_size);
    alloc_spline(ctx, &s[1], &delay, current_delay, &span_size, current_span_size);
  }
  delay_spline(ctx, s[0], A_LEFT,  -DELAY_XOFFSET);
  delay_spline(ctx, s[1], A_RIGHT, +DELAY_XOFFSET);
  DelayS_draw(ctx);
#ifdef PARTICLES
  DelayS_particles(ctx);
#endif
}
