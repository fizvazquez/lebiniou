/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DELAY_H
#define __DELAY_H

// to separate left and right channels
#define DELAY_XOFFSET 0.5


void
alloc_spline(const Context_t *ctx, Spline_t **s, uint8_t *delay, const uint8_t new_delay, uint8_t *span_size, const uint8_t new_span_size)
{
  if (*s != NULL) {
    Spline_delete(*s);
  }
  uint32_t samples = Context_get_phase_space_samples(ctx);
  printf("[i] (re)allocating spline (delay: %d, span: %d, samples: %d)\n", new_delay, new_span_size, samples);
  *s = Spline_new(new_span_size, samples);
  Spline_info(*s);
  if (delay != NULL) {
    *delay = new_delay;
  }
  if (span_size != NULL) {
    *span_size = new_span_size;
  }
}


static void
delay_spline(Context_t *ctx, Spline_t *s, const enum Channel channel, const float x_offset)
{
  Input_t *input = ctx->input;
  const uint8_t delay = Context_get_phase_space_delay(ctx);
  uint32_t x = 0;
  uint32_t y = delay;
  uint32_t z = 2 * delay;

  pthread_mutex_lock(&input->mutex);
  for ( ; z < input->size; x++, y++, z++) {
    s->cpoints[x].pos.x = input->data[channel][x] + x_offset;
    s->cpoints[x].pos.y = input->data[channel][y] + x_offset;
    s->cpoints[x].pos.z = input->data[channel][z] + x_offset;
  }
  pthread_mutex_unlock(&input->mutex);
  Spline_compute(s);
}

#endif /* __DELAY_H */
