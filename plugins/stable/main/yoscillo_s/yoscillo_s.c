/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "oscillo.h"


u_long id = 1089152275;
u_long options = BE_SFX2D|BEQ_VER;
char dname[] = "Y oscillo stereo";
u_long mode = OVERLAY;
char desc[] = "Vertical stereo oscilloscope";

static Porteuse_t *PL = NULL, *PR = NULL;
static int connect = 1;


static void
init()
{
  uint32_t i;
  Transform_t t;

  memset(&t, 0, sizeof(t));

  PL->origin.y = PR->origin.y = 0;

  PL->origin.x = HWIDTH/2;
  PR->origin.x = MAXX-HWIDTH/2;

  t.v_j_factor = HMAXX * 0.85;
  t.v_i.y = 1.0 / (float)(PL->size - 1) * (float)MAXY;

  for (i = 0; i < PL->size; i++) {
    PL->trans[i] = PR->trans[i] = t;
  }

  Porteuse_init_alpha(PL);
  Porteuse_init_alpha(PR);
}


void
create(Context_t *ctx)
{
  if (ctx->input != NULL) {
    PL = Porteuse_new(ctx->input->size, A_LEFT);
    PR = Porteuse_new(ctx->input->size, A_RIGHT);
    init();
  } else {
    options |= BEQ_DISABLED;
  }
}


void
destroy(Context_t *ctx)
{
  if (ctx->input != NULL) {
    Porteuse_delete(PL);
    Porteuse_delete(PR);
  }
}


void
on_switch_on(Context_t *ctx)
{
  /* connect = b_rand_boolean(); */
}


void
run(Context_t *ctx)
{
  Buffer8_clear(passive_buffer(ctx));
  Porteuse_draw(PL, ctx, connect);
  Porteuse_draw(PR, ctx, connect);
}
