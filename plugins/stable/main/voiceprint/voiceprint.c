/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


u_long id = 945291309;
u_long options = BE_SFX2D|BEQ_NORANDOM;
u_long mode = OVERLAY;
char desc[] = "Voiceprint effect";


static Buffer8_t *my_scr = NULL;
static u_short *v_start = NULL, *v_end = NULL;


static void
init_v(Context_t *ctx)
{
  u_short k;
  float da_log;

  da_log = logf(ctx->input->spectrum_size - 1) / logf(10.0);

  for (k = 1; k < ctx->input->spectrum_size; k++) {
    v_start[k] = (u_short)(logf((float)k)/logf(10.0) / da_log * MAXY);
    v_end[k]   = (u_short)(log1p((float)k)/logf(10.0) / da_log * MAXY); /* log1p(x)=logf(x+1) */
  }
}


void
run(Context_t *ctx)
{
  Buffer8_t *src = my_scr;
  Buffer8_t *dst = passive_buffer(ctx);
  u_short k;

  memmove((void *)src->buffer, (const void *)(src->buffer+sizeof(Pixel_t)), BUFFSIZE-1);

  pthread_mutex_lock(&ctx->input->mutex);
  for (k = 1; k < ctx->input->spectrum_size; k++) {
    Pixel_t color1 = (Pixel_t)(255.0 * ctx->input->spectrum_log[A_MONO][k]);
    v_line_nc(src, MAXX, v_start[k], v_end[k] - 1, color1);
  }
  pthread_mutex_unlock(&ctx->input->mutex);

  v_line_nc(src, MINX, MINY, MAXY, 0);

  Buffer8_copy(src, dst);
  //  Buffer8_add(src, dst, 16);
  //  Buffer8_clear_border(dst);
  //  Buffer8_copy(active_buffer(ctx->biniou8), passive_buffer(ctx->biniou8));
}


void
create(Context_t *ctx)
{
  if (ctx->input != NULL) {
    v_start = xcalloc(ctx->input->spectrum_size, sizeof(u_short));
    v_end = xcalloc(ctx->input->spectrum_size, sizeof(u_short));

    my_scr = Buffer8_new();

    init_v(ctx);
  } else {
    options |= BEQ_DISABLED;
  }
}


void
destroy(Context_t *ctx)
{
  Buffer8_delete(my_scr);
  xfree(v_start);
  xfree(v_end);
}
