/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "../include/infinity.h"


u_long id = 1188927899;
u_long options = BE_DISPLACE;
char dname[] = "Infinity";
char desc[] = "Infinity effect";


/* Infinity plugin port.
 * Original source code has been heavily modified, to take only
 * the vector fields. So modified it's nearly a rewrite.
 * Changes have also been made to reflect style(9).
 * See the original infinity plugin source code for exact details
 */
#define NB_FCT 6


static t_complex
fct(t_complex a, guint32 n, gint32 p1, gint32 p2)   /* p1 et p2:0-4 */
{
  t_complex b;
  gfloat fact;
  gfloat an;
  gfloat circle_size;
  gfloat speed;
  gfloat co,si;

  a.x -= HWIDTH;
  a.y -= HHEIGHT;

  switch (n) {
  case 0:
    an = 0.025*(p1-2)+0.002;
    co = cosf(an);
    si = sinf(an);
    circle_size = HEIGHT*0.25;
    speed = (gfloat)2000+p2*500;
    b.x = (co*a.x-si*a.y);
    b.y = (si*a.x+co*a.y);
    fact = -(sqrtf(b.x*b.x+b.y*b.y)-circle_size)/speed+1;
    b.x = (b.x*fact);
    b.y = (b.y*fact);
    break;

  case 1:
    an = 0.015*(p1-2)+0.002;
    co = cosf(an);
    si = sinf(an);
    circle_size = HEIGHT*0.45;
    speed = (gfloat)4000+p2*1000;
    b.x = (co*a.x-si*a.y);
    b.y = (si*a.x+co*a.y);
    fact = (sqrtf(b.x*b.x+b.y*b.y)-circle_size)/speed+1;
    b.x = (b.x*fact);
    b.y = (b.y*fact);
    break;

  case 2:
    an = 0.002;
    co = cosf(an);
    si = sinf(an);
    circle_size = HEIGHT*0.25;
    speed = (gfloat)400+p2*100;
    b.x = (co*a.x-si*a.y);
    b.y = (si*a.x+co*a.y);
    fact = -(sqrtf(b.x*b.x+b.y*b.y)-circle_size)/speed+1;
    b.x = (b.x*fact);
    b.y = (b.y*fact);
    break;

  case 3:
    an = (sinf(sqrtf(a.x*a.x+a.y*a.y)/20)/20)+0.002;
    co = cosf(an);
    si = sinf(an);
    circle_size = HEIGHT*0.25;
    speed = (gfloat)4000;
    b.x = (co*a.x-si*a.y);
    b.y = (si*a.x+co*a.y);
    fact = -(sqrtf(b.x*b.x+b.y*b.y)-circle_size)/speed+1;
    b.x = (b.x*fact);
    b.y = (b.y*fact);
    break;

  case 4:
    an = 0.002;
    co = cosf(an);
    si = sinf(an);
    circle_size = HEIGHT*0.25;
    speed = sinf(sqrtf(a.x*a.x+a.y*a.y)/5)*3000+4000;
    b.x = (co*a.x-si*a.y);
    b.y = (si*a.x+co*a.y);
    fact = -(sqrtf(b.x*b.x+b.y*b.y)-circle_size)/speed+1;
    b.x = (b.x*fact);
    b.y = (b.y*fact);
    break;

  case 5:
    an = 0.002;
    co = cosf(an);
    si = sinf(an);
    circle_size = HEIGHT*0.25;
    fact = 1+cosf(atanf(a.x/(a.y+0.00001))*6)*0.02;
    b.x = (co*a.x-si*a.y);
    b.y = (si*a.x+co*a.y);
    b.x = (b.x*fact);
    b.y = (b.y*fact);
    break;

  default:
    b.x = 0.0;
    b.y = 0.0;
  }

  b.x += HWIDTH;
  b.y += HHEIGHT;

  if (b.x < 0.0 ) {
    b.x = 0.0;
  }
  if (b.y < 0.0) {
    b.y = 0.0;
  }
  if (b.x > (gfloat)MAXX) {
    b.x = (gfloat)MAXX;
  }
  if (b.y > (gfloat)MAXY) {
    b.y = (gfloat)MAXY;
  }

  return b;
}


/* Biniou plugin callbacks */
static u_char num_effect = 0;
static BTimer_t *timer = NULL;
static Shuffler_t *shuffler = NULL;
static VectorField_t *vf = NULL;


void
create(Context_t *ctx)
{
  vf = VectorField_new(NB_FCT, &fct);
  timer = b_timer_new();
  shuffler = Shuffler_new(NB_FCT);
}


void
destroy(Context_t *ctx)
{
  VectorField_delete(vf);
  b_timer_delete(timer);
  Shuffler_delete(shuffler);
}


void
on_switch_on(Context_t *ctx)
{
  num_effect = Shuffler_get(shuffler);
  b_timer_start(timer);
}


void
run(Context_t *ctx)
{
  /* printf("oOo %s:%d: effect= %d\n", __FILE__, __LINE__, num_effect); */
  VectorField_run(vf, ctx, num_effect);

  /* TODO remove hardcoded 5 seconds delay */
  if (b_timer_elapsed(timer) > 5) {
    on_switch_on(ctx);
  }
}
