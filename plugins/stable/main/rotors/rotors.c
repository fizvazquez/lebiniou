/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This plugin in memoriam of Laurent "Lolo" Fileux.
 * Wherever you are now, it was a great experience hacking
 * with you. See you soon.
 */

#include "context.h"

u_long id = 949178536;
u_long options = BE_GFX;
u_long mode = OVERLAY;
char desc[] = "Rotors effect";


#define MAX_ROTORS 16

typedef struct Rotor {
  struct Rotor *fg, *fd;
  _Complex float centre;
  float rayon, freq, freq_var, ampl_var, freq_coul;
  u_char visible;
  Pixel_t coul;
} Rotor;

/*
 * float min_rnd_factor, max_rnd_factor;
 u_short length, nb_rotors, min_color, max_color;
 float freq_base_moy, freq_base_ect;
 float freq_var_moy, freq_var_ect;
 float ampl_var_moy, ampl_var_ect;
 float proba_visible, rotor_time;
 int max_prof;

 Rotor tab[MAX_ROTORS];
 float time_step;
*/

static _Complex float ci;
static float min_rnd_factor, max_rnd_factor;
static u_short length, nb_rotors, min_color, max_color;
static float freq_base_moy, freq_base_ect;
static float freq_var_moy, freq_var_ect;
static float ampl_var_moy, ampl_var_ect;
static float proba_visible, rotor_time;
static int max_prof;

static Rotor tab[MAX_ROTORS];
static float time_step;


/* note: this is _NOT_ a perfect binary-tree,
 * ie a node might have 1 or 2 sons
 */
static int
prof_rotor_tree(Rotor *rotor)
{
  int pg, pd;

  if (rotor == NULL) {
    return 0;
  }

  pg = prof_rotor_tree(rotor->fg);
  pd = prof_rotor_tree(rotor->fd);

  return 1 + ((pg > pd) ? pg : pd);
}


static void
build_rotor_tree()
{
  int i;
  int *arite;

  for (i = 0; i < nb_rotors; i++) {
    tab[i].fg = tab[i].fd = NULL;
  }

  arite = xcalloc(nb_rotors, sizeof(int));
  arite[0] = 2;

  for (i = 1; i < nb_rotors; i++) {
    /* Tirage au sort du pere a la roulette */
    int hasard = (int)(drand48 () * i), j = 0;

    while ((j < i) && (hasard >= 0)) {
      hasard -= arite[j];
      j++;
    }
    j--;

    /* Le resultat de la roulette est dans 'j' */
    arite[j]--;
    if (tab[j].fg == NULL) {
      tab[j].fg = tab + i;
    } else if (tab[j].fd == NULL) {
      tab[j].fd = tab + i;
    }
    arite[i] = 2;
  }

  xfree(arite);
  max_prof = prof_rotor_tree(tab);
}


static void
init_rotor_tree(Rotor *rotor, float max_ray)
{
  int prof = prof_rotor_tree(rotor);

  rotor->rayon = (drand48() * (max_rnd_factor - min_rnd_factor) + min_rnd_factor) * (float)max_ray / (float)prof;
  if (rotor->fg != NULL) {
    init_rotor_tree(rotor->fg, max_ray - rotor->rayon);
  }
  if (rotor->fd != NULL) {
    init_rotor_tree(rotor->fd, max_ray - rotor->rayon);
  }
}


static void
build()
{
  build_rotor_tree();
  init_rotor_tree(tab, MINSCREEN);
}


static inline float
norm_freq()
{
  float d = (float)(drand48() * 2.0 - 1.0);
  return freq_var_moy + freq_var_ect * d;
}


static inline float
norm_ampl()
{
  float d = (float)(drand48() * 2.0 - 1.0);
  return ampl_var_moy + ampl_var_ect * d;
}


static inline float
norm_freq_base()
{
  float d = (float)(drand48() * 2.0 - 1.0);
  return freq_base_moy + freq_base_ect * d;
}


static inline Pixel_t
couleur()
{
  float d = drand48() * (max_color - min_color) + min_color;
  return (Pixel_t)d;
}


static void
set_random_visible()
{
  int i;

  VERBOSE(printf("[r] Building rotors: "));
  for (i = 0; i < nb_rotors; i++)  {
    tab[i].visible = (drand48() <= proba_visible);
    if (tab[i].visible) {
      VERBOSE(printf("+"));
    } else {
      VERBOSE(printf("-"));
    }
  }
  VERBOSE(printf(" done\n"));
}


static void
post_init()
{
  int i;

  for (i = 0; i < nb_rotors; i++)  {
    tab[i].coul      = couleur();
    tab[i].freq      = norm_freq_base();
    tab[i].freq_var  = norm_freq();
    tab[i].ampl_var  = norm_ampl();
  }
  set_random_visible();

  rotor_time = 0;
  build();
}


void
create(Context_t *ctx)
{
  ci = cexp(I*M_PI/2);
  /* printf("Rotors:  %f+%f*i\n", creal(ci), cimag(ci)); */

  nb_rotors = 16;
  length = 200;
  min_color = 200;
  max_color = 250;
  min_rnd_factor = 0.3;
  max_rnd_factor = 2;
  freq_base_moy = 0;
  freq_base_ect = 70;
  freq_var_moy = 0.6;
  freq_var_ect = 0.005;
  ampl_var_moy = 0;
  ampl_var_ect = 0.1;
  proba_visible = 0.333;
  time_step = 0.00001;

  post_init();
}


static void
refresh(Rotor *rotor)
{
  if ((rotor->fg != NULL) || (rotor->fd != NULL))  {
    float f, arg, alpha;
    _Complex float pos_rel;

    f = rotor->freq
        * exp(rotor->ampl_var * cos(2 * M_PI * rotor->freq_var * rotor_time));
    arg = f * rotor_time;
    arg -= (long)(arg);
    alpha = 2 * M_PI * arg;

    pos_rel = rotor->rayon * cexp(ci * alpha);

    if (rotor->fg != NULL) {
      rotor->fg->centre = rotor->centre + pos_rel;
      refresh(rotor->fg);
    }
    if (rotor->fd != NULL) {
      rotor->fd->centre = rotor->centre - pos_rel;
      refresh(rotor->fd);
    }
  }
}


static void
display(Context_t *ctx)
{
  u_short i;
  Buffer8_t *dst = passive_buffer(ctx);

  for (i = 7; i < MAX_ROTORS; i++)
    /* pas la racine ni les 2 premiers niveaux -> 1+2+4 rotors non affiches */
    if (tab[i].visible) {
      short x, y;
      x = (short)(creal(tab[i].centre) + CENTERX);
      y = (short)(cimag(tab[i].centre) + CENTERY);
      /*      printf ("x= %d\ty= %d\n", x, y);*/

#if 1
      set_pixel(dst, x, y, tab[i].coul);
#else /* testing neg_pixel, if it's nice we can cleanup tab[i].coul etc */
      neg_pixel(dst, x, y);
#endif
    }
}

void
run(Context_t *ctx)
{
  int i;

  Buffer8_clear(passive_buffer(ctx));

  for (i = 0; i < length; i++) {
    rotor_time += time_step;
    refresh(tab);
    display(ctx);
  }
}

/*
  SAMPLE config from the v1.O

  ROTORS 12
  MIN_COLOR 200
  MAX_COLOR 250
  MIN_RND_FACTOR 0.3
  MAX_RND_FACTOR 2
  FRQ_BASE_MOY 0
  FRQ_BASE_ECT 70
  FRQ_VARIANCE_MOY 0.6
  FRQ_VARIANCE_ECT 0.005
  AMP_VARIANCE_MOY 0
  AMP_VARIANCE_ECT 0.1
  PROBA_VISIBLE 0.15
  TIME_STEP 0.00001
  TRAIL_LENGTH 200

*/

void
on_switch_on(Context_t *ctx)
{
  post_init();
}
