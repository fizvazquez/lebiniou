/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "spline.h"
#include "particles.h"

u_long id = 1194552390;
u_long options = BE_SFX3D|BEQ_PARTICLES|BEQ_NORANDOM;
u_long mode = OVERLAY;
char desc[] = "Fountain effect";


#define CONNECT 1

static Spline_t *s = NULL;
static Particle_System_t *ps = NULL;
static const Point3d_t ORIGIN = { { 0.0, -1.0, 0.0 } };


static void
Delay3_particles(Context_t *ctx)
{
  u_short i;
  Input_t *input = ctx->input;
  Buffer8_t *dst = passive_buffer(ctx);

  Buffer8_clear(dst);
  Particle_System_go(ps);

  for (i = 0; (i < s->nb_spoints) && Particle_System_can_add(ps); i++) {
    Particle_t *p = NULL;
    float ttl = Input_random_float_range(input, 0.5, 0.8);
    Point3d_t source;
    Pixel_t col;

    source.pos.x = source.pos.z = 0;
    source.pos.y = Input_random_float_range(input, -1.1, -0.9);
    col = Input_random_color(input);

    /* TODO si c'est ok *= 0.25 lors de la generation des points de controle
     * de la spline */
    s->spoints[i].pos.x *= 0.2;
    s->spoints[i].pos.z *= 0.2;
    s->spoints[i].pos.y = fabs(s->spoints[i].pos.y) + 0.1;
    p = Particle_new_indexed(ttl, col, source, /*p3d_mul(&*/s->spoints[i]/*, 0.25)*/, ORIGIN, -0.5);

    Particle_System_add(ps, p);
  }

  Particle_System_draw(ps, &ctx->params3d, dst);
}


static void
Delay3_init(Context_t *ctx)
{
  u_short i;

  pthread_mutex_lock(&ctx->input->mutex);

  /* Map cubique (x y z) */
  s->cpoints[0].pos.x = ctx->input->data[A_MONO][0];
  s->cpoints[0].pos.y = ctx->input->data[A_MONO][1];
  s->cpoints[0].pos.z = ctx->input->data[A_MONO][2];

  for (i = 1; i < s->nb_cpoints; i++) {
    s->cpoints[i].pos.x = s->cpoints[i-1].pos.y;
    s->cpoints[i].pos.y = s->cpoints[i-1].pos.z;
    s->cpoints[i].pos.z = ctx->input->data[A_MONO][i+2];
  }

  pthread_mutex_unlock(&ctx->input->mutex);
}


void
create(Context_t *ctx)
{
  if (ctx->input == NULL) {
    options |= BEQ_DISABLED;
  } else {
    ps = Particle_System_new(PS_NOLIMIT);

    s = Spline_new(SPAN_SIZE, ctx->input->size - 2);
#ifdef DEBUG
    Spline_info(s);
#endif
  }
}


void
destroy(Context_t *ctx)
{
  if (ps != NULL) {
    Particle_System_delete(ps);
  }
  if (s != NULL) {
    Spline_delete(s);
  }
}


void
run(Context_t *ctx)
{
  if (Plugin_is_enabled(options)) {
    Delay3_init(ctx);
    Spline_compute(s);
    Delay3_particles(ctx);
  }
}
