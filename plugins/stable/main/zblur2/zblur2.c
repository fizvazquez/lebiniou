/*
 *  Copyright 2014-2019 Frantz Balinski
 *  Copyright 2018-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * l'idée :
 *   utiliser une pondération simulant une sphère 3x3:
 *     1 2 1
 *     2 4 2
 *     1 2 1
 *   total = 1+2+1 + 2+4+2 + 1+2+1 = 16
 */

#include "context.h"

u_long id = 1403972381;
u_long options = BE_BLUR;
char desc[] = "Spherical blur (3x3)";


#define CONV(x,n)  ((u_short) (x) << (n))


void
run(Context_t *ctx)
{
  Buffer8_t *dst = passive_buffer(ctx);
  const Pixel_t *Y0X0, *Y0X1, *Y0X2,
        *Y1X0, *Y1X1, *Y1X2,
        *Y2X0, *Y2X1, *Y2X2;
  u_short i, j;

  Buffer8_init_mask_3x3(active_buffer(ctx));

  // sources
  Y0X0 = active_buffer(ctx)->buffer;  // < 0, 0 >, weight 1 (<< 0)
  Y0X1 = Y0X0 + 1;                    // < 1, 0 >, weight 2 (<< 1)
  Y0X2 = Y0X1 + 1;                    // < 2, 0 >, weight 1 (<< 0)
  Y1X0 = Y0X0 + WIDTH;                // < 0, 1 >, weight 2 (<< 1)
  Y1X1 = Y1X0 + 1;                    // < 1, 1 >, weight 4 (<< 2)
  Y1X2 = Y1X1 + 1;                    // < 2, 1 >, weight 2 (<< 1)
  Y2X0 = Y1X0 + WIDTH;                // < 0, 2 >, weight 1 (<< 0)
  Y2X1 = Y2X0 + 1;                    // < 1, 2 >, weight 2 (<< 1)
  Y2X2 = Y2X1 + 1;                    // < 2, 2 >, weight 1 (<< 0)

  // destination
  Pixel_t *b8 = dst->buffer + WIDTH + 1;

  for (j = 1; j < MAXY; j++) {
    for (i = 1; i < MAXX; i++) {
      *b8++ = (Pixel_t) ((  CONV(*Y0X0++, 0) + CONV(*Y0X1++, 1) + CONV(*Y0X2++, 0)
                            + CONV(*Y1X0++, 1) + CONV(*Y1X1++, 2) + CONV(*Y1X2++, 1)
                            + CONV(*Y2X0++, 0) + CONV(*Y2X1++, 1) + CONV(*Y2X2++, 0)) >> 4);
    }
    Y0X0 += 2, Y0X1 += 2, Y0X2 += 2;
    Y1X0 += 2, Y1X1 += 2, Y1X2 += 2;
    Y2X0 += 2, Y2X1 += 2, Y2X2 += 2;
    b8 += 2;
  }

  Buffer8_expand_border(dst);
}
