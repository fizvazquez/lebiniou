/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


u_long id = 1547125655;
u_long options = BE_SFX3D;
u_long mode = OVERLAY;
char desc[] = "Phase-space reconstruction";


void
create(Context_t *ctx)
{
  if (NULL == ctx->input) {
    options |= BEQ_DISABLED;
  }
}


void
run(Context_t *ctx)
{
  Buffer8_t *dst = passive_buffer(ctx);
  const Params3d_t *params3d = &ctx->params3d;
  Input_t *input = ctx->input;
  const uint8_t delay = Context_get_phase_space_delay(ctx);
  uint32_t x1 = 0;
  uint32_t y1 = delay;
  uint32_t z1 = 2 * delay;
  uint32_t z2 = 3 * delay;

  Buffer8_clear(dst);

  pthread_mutex_lock(&input->mutex);
  for ( ; z2 < input->size; x1++, y1++, z1++, z2++) {
    Point3d_t p1, p2;
    p1.pos.x = input->data[A_MONO][x1];
    p1.pos.y = input->data[A_MONO][y1];
    p1.pos.z = input->data[A_MONO][z1];
    p2.pos.x = input->data[A_MONO][y1];
    p2.pos.y = input->data[A_MONO][z1];
    p2.pos.z = input->data[A_MONO][z2];
    Pixel_t color = Input_random_color(input);
    draw_line_3d(params3d, dst, &p1, &p2, color);
  }
  pthread_mutex_unlock(&input->mutex);
}
