/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL2/SDL.h>
#include "biniou.h"
#include "images.h"
#include "colormaps.h"
#include "osd.h"
#include "ttf.h"
#include "src/defaults.h"

#define PB_WIDTH 3 /* progress bar width (%) */
#define SHOW 18    /* number of plugins to display before/after the current plugin */

extern char has_osd;
extern u_short fontlineskip;

SDL_Window *osd_window = NULL;

#define BORDER      5
#define ARROW       "->"
#define OSD_BUFFLEN 512

#define OSD_WIDTH  600
#define OSD_HEIGHT 900


void osd_init()
{
  char *icon_file;
  SDL_Surface *icon = NULL;
  Uint32 colorkey;
  int flags = SDL_WINDOW_RESIZABLE;

  ttf_init();

  if (!has_osd) {
    return;
  }

  osd_window = SDL_CreateWindow("Le Biniou", 0, 0, OSD_WIDTH, OSD_HEIGHT, flags);
  if (NULL == osd_window) {
    xerror("Couldn't set %dx%d video mode: %s\n", OSD_WIDTH, OSD_HEIGHT, SDL_GetError());
  }

  icon_file = g_strdup_printf("%s/lebiniou.bmp", DEFAULT_DATADIR);
  icon = SDL_LoadBMP(icon_file);
  g_free(icon_file);
  colorkey = SDL_MapRGB(icon->format, 0, 0, 0);
  SDL_SetColorKey(icon, SDL_TRUE, colorkey);
  SDL_SetWindowIcon(osd_window, icon);
  SDL_FreeSurface(icon);
}


void osd_quit()
{
  if (has_osd) {
    SDL_DestroyWindow(osd_window);
    ttf_quit();
  }
}


static void
osd_info(const Context_t *ctx)
{
  char buff[OSD_BUFFLEN+1];
  int dst_y = 0;

  struct timeval now;
  char *now_str;

  float elapsed;
  u_short d, h, m, s;

  const Sequence_t *cur_seq = ctx->sm->cur;

  /* Display readable localtime */
  /* TODO error checking */
  gettimeofday(&now, NULL);
  /* XXX TODO error + bounds checking */
  now_str = ctime((time_t *)&now.tv_sec);
  now_str[strlen(now_str)-1] = '\0';

  dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "%s", now_str);

  /* Uptime */
  elapsed = b_timer_elapsed(ctx->timer);
  d = (u_short)(elapsed / (3600*24));
  elapsed -= d*3600*24;
  h = (u_short)(elapsed / 3600);
  elapsed -= h*3600;
  m = (u_short)(elapsed / 60);
  elapsed -= m*60;
  s = (u_short)elapsed;
  elapsed -= s;
  dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Up: %1dd %02d:%02d:%02d.%02d",
                    d, h, m, s, (u_short)(elapsed*100));

  /* Display sequence name */
  dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Sequence: %s",
                    (cur_seq->name != NULL) ? cur_seq->name : "(none)");

  /* Display current bankset:bank */
  switch (ctx->bank_mode) {
  case SEQUENCES:
    dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Sequences bank: %d-%d",
                      ctx->bankset[SEQUENCES]+1, ctx->bank[SEQUENCES]+1);
    break;
  case COLORMAPS:
    dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Colormaps bank: %d-%d",
                      ctx->bankset[COLORMAPS]+1, ctx->bank[COLORMAPS]+1);
    break;
  case IMAGES:
    dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Images bank: %d-%d",
                      ctx->bankset[IMAGES]+1, ctx->bank[IMAGES]+1);
    break;
  }

  /* Display colormap and image (if any) */
  assert(colormaps != NULL);
  dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Colormap: %s",
                    (cur_seq->cmap_id) ? Colormaps_name(cur_seq->cmap_id) : "(default)");

  if (NULL != images)
    dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Image: %s",
                      (cur_seq->image_id) ? Images_name(cur_seq->image_id) : "(default)");

  /* Display auto* stuff */
  if (ctx->random_mode != BR_NONE) {
    const char *what = NULL;

    if (ctx->random_mode == BR_SCHEMES) {
      what = "Schemes";
    } else if (ctx->random_mode == BR_SEQUENCES) {
      what = "Sequences";
    } else if (ctx->random_mode == BR_BOTH) {
      what = "Schemes+Sequences";
    }
    snprintf(buff, OSD_BUFFLEN * sizeof(char), "Auto mode: %s", what);
  } else {
    snprintf(buff, OSD_BUFFLEN * sizeof(char), "Auto mode: Off");
  }
  dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "%s", buff);

  /* Display random cmap/image */
  if ((colormaps != NULL) && (colormaps->size > 1))
    dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Random colormaps: %s",
                      ctx->auto_colormaps ? "On" : "Off");

  if ((images != NULL) && (images->size > 1))
    dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Random images: %s",
                      ctx->auto_images ? "On" : "Off");

  /* Display locked plugin, if any */
  if (ctx->locked != NULL) {
    dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Locked: %s", Plugin_dname(ctx->locked));
  }

  /* Display phase-space delay */
  dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Phase-space delay: %d", Context_get_phase_space_delay(ctx));

  /* Spline span size */
  dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Span size: %d", Context_get_span_size(ctx));

  /* Volume scaling */
  if (ctx->input != NULL) {
    dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "Volume scale: %.1f", Context_get_volume_scale(ctx));
  }

  /* 3D scale factor */
  dst_y = osd_print(BORDER, dst_y, 0, 0, 0, "3D scale factor: %.2f", ctx->params3d.scale_factor);
}


static void
osd_fps(const Context_t *ctx)
{
  (void)osd_print(BORDER, 0, 1, 1, 0, "%03d FPS (%03d)", (int)Context_fps(ctx), ctx->max_fps);
}


static void
osd_sequence(const Context_t *ctx)
{
  char buff[OSD_BUFFLEN+1];
  const Sequence_t *cur_seq;
  GList *tmp;
  u_short dst_y = 0;
  u_char lens_there = 0;

  cur_seq = ctx->sm->cur;
  tmp = g_list_first(cur_seq->layers);

  while (tmp != NULL) {
    Layer_t *layer = (Layer_t *)tmp->data;
    Plugin_t *P = layer->plugin;

    if (P != NULL) {
      char *name = Plugin_dname(P);
      const char *mode = LayerMode_to_OSD_string(layer->mode);
      const char *arrow = (P == plugins->selected) ? ARROW : "";

      if ((cur_seq->lens != NULL) && (P == cur_seq->lens)) {
        lens_there = 1;
        snprintf(buff, OSD_BUFFLEN*sizeof(char), "%s %s - %s", arrow, name, mode);
      } else {
        const char lens_there_c = (lens_there) ? ' ' : '|';
        snprintf(buff, OSD_BUFFLEN*sizeof(char), "%s %s %c %s", arrow, name, lens_there_c, mode);
      }
      xfree(name);

      dst_y = osd_print(BORDER, dst_y, 1, 0, 0, "%s", buff);
    }
    tmp = g_list_next(tmp);
  }

  cur_seq = ctx->sm->cur;
}


static void
osd_plugins(const Context_t *ctx)
{
  short n;
  short start;
  u_short skip = fontlineskip - 1;
  u_short dst_y = 2 * SHOW * skip + 2 * skip;

  start = plugins->selected_idx - SHOW;
  while (start < 0) {
    start += plugins->size;
  }

  for (n = 0; (n < 2 * SHOW + 1) && (n < plugins->size); ) {
    const char *arrow;
    char in_sequence;
    char *name;
    Plugin_t *plugin = plugins->plugins[start];

    arrow = (n == SHOW) ? ARROW : "  ";
    in_sequence = Sequence_find(ctx->sm->cur, plugin) ? '*' : ' ';

    name = Plugin_dname(plugin);
    (void)osd_print(BORDER, dst_y, 0, 1, (*plugin->options & BEQ_DISABLED) ? 1 : 0, "%c|%c|%c|%c|%c %s %c %s",
                    ((*plugin->options & BE_SFX2D) || (*plugin->options & BE_SFX3D)) ? 'S' : ' ',
                    (*plugin->options & BE_GFX)  ? 'G' : ' ',
                    ((*plugin->options & BE_BLUR) || (*plugin->options & BE_DISPLACE)
                     || (*plugin->options & BE_ROLL) || (*plugin->options & BE_WARP)
                     || (*plugin->options & BE_SCROLL) || (*plugin->options & BE_MIRROR))  ? 'F' : ' ',
                    ((*plugin->options & BEQ_IMAGE) || (*plugin->options & BEQ_SPLASH))  ? 'I' : ' ',
                    (*plugin->options & BE_LENS)  ? 'L' : ' ',
                    arrow, in_sequence, name);
    xfree(name);
    dst_y -= skip;
    n++;

    start++;
    if (start == plugins->size) {
      start = 0;
    }
  }
}


static void
osd_plugin_desc(const Context_t *ctx)
{
  char *dsc = NULL;
  int skip = fontlineskip - 1;

  if (NULL == plugins->selected->desc) {
    dsc = "NO DESCRIPTION";
  } else {
    dsc = plugins->selected->desc;
  }

  (void)osd_print(BORDER, skip, 1, 1, 0, "%s", dsc);
}


static void
osd_random_mode_elapsed(const Context_t *ctx)
{
  float pct = Alarm_elapsed_pct(ctx->a_random);
  u_char color = 255; /* TODO colormap->max */
  u_short width;
  u_short height;
  SDL_Rect r;

  int w, h;
  SDL_GetWindowSize(osd_window, &w, &h);

  height = (u_short)((1.0 - pct) * h);
  width = (u_short)(w * PB_WIDTH / 100.0);
  r.x = w - width;
  r.y = h - height;
  r.w = width;
  r.h = height;

  SDL_FillRect(SDL_GetWindowSurface(osd_window), &r, color);
}


void
osd(const Context_t *ctx)
{
  SDL_Rect r;
  int w, h;

  SDL_GetWindowSize(osd_window, &w, &h);
  r.x = r.y = 0;
  r.w = w;
  r.h = h;
  SDL_FillRect(SDL_GetWindowSurface(osd_window), &r, 0);

  osd_info(ctx);

  if (ctx->random_mode != BR_NONE) {
    osd_random_mode_elapsed(ctx);
  }

  if (ctx->sync_fps) {
    osd_fps(ctx);
  }

  osd_sequence(ctx);

  osd_plugins(ctx);

  osd_plugin_desc(ctx);

  if (SDL_UpdateWindowSurface(osd_window) < 0) {
    SDL_Log("SDL_UpdateWindowSurface failed: %s", SDL_GetError());
    exit(1);
  }
}
