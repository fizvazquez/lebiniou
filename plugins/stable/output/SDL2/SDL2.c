/*
 *  Copyright 2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL2/SDL.h>
#include "biniou.h"
#include "osd.h"
#include "src/defaults.h"
#include "events.h"

u_long id = 1541681431;
u_long options = BE_NONE;

#define NO_MOUSE_CURSOR

SDL_Window *window = NULL;
char has_osd = 1;
static SDL_DisplayMode current; // current screen resolution


static void
create_window(const Uint32 flags)
{
  char *icon_file;
  SDL_Surface *icon = NULL;
  Uint32 colorkey;
  char *window_title;

  window_title = g_strdup_printf("Le Biniou (%dx%d)", WIDTH, HEIGHT);
  window = SDL_CreateWindow(window_title, current.w - WIDTH, 0, WIDTH, HEIGHT, flags);
  g_free(window_title);
  if (NULL == window) {
    xerror("Couldn't set %dx%d video mode: %s\n", WIDTH, HEIGHT, SDL_GetError());
  }

  icon_file = g_strdup_printf("%s/lebiniou.bmp", DEFAULT_DATADIR);
  icon = SDL_LoadBMP(icon_file);
  g_free(icon_file);
  colorkey = SDL_MapRGB(icon->format, 0, 0, 0);
  SDL_SetColorKey(icon, SDL_TRUE, colorkey);
  SDL_SetWindowIcon(window, icon);
  SDL_FreeSurface(icon);
}


static inline void
SDL_refresh_32bpp(Context_t *ctx, SDL_Surface *sc)
{
  const RGBA_t *src;

  Buffer8_flip_v(active_buffer(ctx));
  src = export_RGBA_active_buffer(ctx);
  Buffer8_flip_v(active_buffer(ctx));

  const int depth = 32, pitch = 4 * WIDTH;
  const Uint32 pixel_format = SDL_PIXELFORMAT_RGBA32;

  SDL_Surface* surf = SDL_CreateRGBSurfaceWithFormatFrom((void *)src, WIDTH, HEIGHT,
                      depth, pitch, pixel_format);
  assert(surf != NULL);
  if (SDL_BlitScaled(surf, NULL, SDL_GetWindowSurface(window), NULL) < 0) {
    xerror("SDL_BlitScaled failed\n");
  }
  SDL_FreeSurface(surf);
}


static void
SDL_get_event(Context_t *ctx)
{
  // TODO middle = change color, right = erase (3x3)

  SDL_Event evt;
  memset(&evt, 0, sizeof(SDL_Event));

  while (SDL_PollEvent(&evt) != 0) {
    BKey_t key;

    switch (evt.type) {
    case SDL_KEYDOWN:
      key.val = evt.key.keysym.sym;
      key.mod = evt.key.keysym.mod;

      on_key(ctx, &key);
      break;

    case SDL_QUIT:
      Context_send_event(ctx, BT_CONTEXT, BC_QUIT, BA_NONE);
      break;

    case SDL_MOUSEMOTION:
      switch (evt.motion.state) {
      case SDL_BUTTON_LEFT:
        ctx->params3d.xe = evt.motion.x;
        ctx->params3d.ye = evt.motion.y;
        Params3d_rotate(&ctx->params3d);
        break;

      case SDL_BUTTON_RIGHT + SDL_BUTTON_LEFT: /* <- WTF ? */
        // printf("right button motion @ %d %d\n",  evt.motion.x, evt.motion.y);
        set_pixel_nc(active_buffer(ctx), evt.motion.x, MAXY-evt.motion.y, 255);
        break;

      default:
        break;
      }
      break;

    case SDL_MOUSEWHEEL:
      if (evt.wheel.y > 0) { // scroll up
	ctx->params3d.scale_factor /= SCALE_FACTOR_MULT;
      } else if (evt.wheel.y < 0) { // scroll down
	if (ctx->params3d.scale_factor > SCALE_FACTOR_MIN)
	  ctx->params3d.scale_factor *= SCALE_FACTOR_MULT;
      }
      printf("[i] 3D scale factor: %.2f\n", ctx->params3d.scale_factor);
      break;

    case SDL_MOUSEBUTTONDOWN:
      /* printf("type= %d, button= %d\n", evt.button.type, evt.button.button); */
      switch (evt.button.button) {
      case SDL_BUTTON_LEFT:
        ctx->params3d.xs = evt.motion.x;
        ctx->params3d.ys = evt.motion.y;
        break;

      case SDL_BUTTON_RIGHT:
        // printf("button down @ %d %d\n",  evt.motion.x, evt.motion.y);
        set_pixel_nc(active_buffer(ctx), evt.motion.x, MAXY-evt.motion.y, 255);
        break;

      default:
        break;
      }
      break;

    default:
      break;
    }
  }
}


void
run(Context_t *ctx)
{
  SDL_refresh_32bpp(ctx, SDL_GetWindowSurface(window));

  if (has_osd) {
    osd(ctx);
  }

  if (SDL_UpdateWindowSurface(window) < 0) {
    SDL_Log("SDL_UpdateWindowSurface failed: %s", SDL_GetError());
    exit(1);
  }

  SDL_get_event(ctx);
}


void
create(Context_t *ctx)
{
  Uint32 flags = 0;
  Uint32 subsystems;
  int ret;

  /* Initialize SDL */
  subsystems = SDL_WasInit(SDL_INIT_VIDEO);
  if (subsystems == 0) {
    ret = SDL_Init(SDL_INIT_VIDEO);
    if (ret == -1) {
      xerror("Couldn't initialize SDL: %s\n", SDL_GetError());
    }
  }

  /* We assume running on the first screen/display */
  const int screen = 0;
  if (SDL_GetCurrentDisplayMode(screen, &current) == 0) {
#ifdef DEBUG
    printf("[i] SDL Screen resolution: %dx%d\n", current.w, current.h);
#endif
  } else {
    xerror("SDL_GetCurrentDisplayMode failed\n");
  }

  osd_init();

#ifndef FIXED
  flags |= SDL_WINDOW_RESIZABLE;
#endif
  create_window(flags);

#ifdef NO_MOUSE_CURSOR
  SDL_ShowCursor(SDL_DISABLE);
#endif
}


void
destroy(Context_t *ctx)
{
  SDL_DestroyWindow(window);
  osd_quit();
  SDL_Quit();
}


void
fullscreen(const int fs)
{
  if (fs) {
    printf("[S] Set full-screen\n");
  } else {
    printf("[S] Unset full-screen\n");
  }
  SDL_SetWindowFullscreen(window, fs ? SDL_WINDOW_FULLSCREEN : 0);
}


void
switch_cursor()
{
  SDL_ShowCursor(SDL_ShowCursor(SDL_QUERY) ? SDL_DISABLE : SDL_ENABLE);
}
