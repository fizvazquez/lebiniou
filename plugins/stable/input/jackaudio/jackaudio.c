/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include <jack/jack.h>
#include <jack/midiport.h>
#include <jack/ringbuffer.h>

#include "midicc_in.h"

// End midi congif impl.

u_long id = 1330135598;
u_long options = BEQ_THREAD;

/* JACK data */
static jack_port_t **input_ports;
static jack_client_t *client;
static char *source_names[2] = { NULL, NULL };
static const char **ports;


#define RBSIZE 100
#define MSG_BUFFER_SIZE 4096

typedef struct {
	uint8_t  buffer[MSG_BUFFER_SIZE];
	uint32_t size;
	uint32_t tme_rel;
	uint64_t tme_mon;
} midimsg;

static jack_port_t* midi_port;
static jack_ringbuffer_t *rb = NULL;
static pthread_cond_t data_ready = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t msg_thread_lock = PTHREAD_MUTEX_INITIALIZER;
static uint64_t monotonic_cnt = 0;
// END MIDI HACK

// default input size if not defined in the configuration file
#define INSIZE 1024
static uint16_t insize = INSIZE;
// buffer jack samples here before copying to input->data
static double *data[2];
// number of jack frames to be read to reach input->size
static uint8_t chunks;


static void
jack_shutdown(void *arg)
{
  Context_t *ctx = (Context_t *)arg;
  printf("[!] JACK: server shut down, exiting\n");
  ctx->running = 0;
}

static command_cc midi_cc[128][128];

static void
execute_event (midimsg* event, Context_t *ctx)
{
	
  if (event->size == 0) {
		return;
	}

	uint8_t type = event->buffer[0] & 0xf0;
	uint8_t channel = event->buffer[0] & 0xf;
  command_cc c;

	switch (type) {
		case 0x90:
			assert (event->size == 3);
			printf ("[i] JACK-MIDI: note on  (channel %2d): pitch %3d, velocity %3d\n", channel, event->buffer[1], event->buffer[2]);
			break;
		case 0x80:
			assert (event->size == 3);
			printf ("[i] JACK-MIDI: note off (channel %2d): pitch %3d, velocity %3d\n", channel, event->buffer[1], event->buffer[2]);
			break;
		case 0xb0:
			assert (event->size == 3);
			printf ("[i] JACK-MIDI: control change (channel %2d): controller %3d, value %3d\n", channel, event->buffer[1], event->buffer[2]);
      if (event->buffer[1] == 32 && event->buffer[2] == 5) {
        Context_send_event(ctx, BT_PLUGINS, BC_PREV, BA_NONE);
      } else if (event->buffer[1] == 32 && event->buffer[2] == 6) {
        Context_send_event(ctx, BT_PLUGINS, BC_NEXT, BA_NONE);
      } else if (event->buffer[1] == 32 && event->buffer[2] == 7) {
        Context_send_event(ctx, BT_CONTEXT, BC_PREV, BA_SEQUENCE);
      } else if (event->buffer[1] == 32 && event->buffer[2] == 8) {
        Context_send_event(ctx, BT_CONTEXT, BC_NEXT, BA_SEQUENCE);
      }

      c = midi_cc[event->buffer[1]][event->buffer[2]];
      if (c.activated == 1) { 
        Context_send_event(ctx, c.subsystem, c.command, c.argument);
      }
			
      break;
		default:
			break;
      
	}
}

static int
process(jack_nframes_t nframes, void *arg)
{
  int chn;
  uint32_t i;
  Context_t *ctx = (Context_t *)arg;
  jack_default_audio_sample_t *in;
  static uint8_t chunk = 0;
  static uint16_t idx = 0;

  if (!ctx->input->mute) {
    uint16_t idx2 = idx;

    for (chn = 0; chn < 2; chn++) {
      in = jack_port_get_buffer(input_ports[chn], nframes);
      if (in != NULL) {
        for (i = 0; i < nframes; i++, idx++) {
          data[chn][idx] = in[i];
        }
        if (chn == 0) {
          idx = idx2;
        }
      }
    }

    chunk++;
#ifdef XDEBUG
    printf("[i] JACK: chunk= %d\n", chunk);
#endif

    if (chunk == chunks) {
#ifdef XDEBUG
      printf("[i] JACK: setting input, idx= %d\n", idx);
#endif
      pthread_mutex_lock(&ctx->input->mutex);
      for (i = 0; i < ctx->input->size; i++) {
        ctx->input->data[A_LEFT][i]  = data[0][i];
        ctx->input->data[A_RIGHT][i] = data[1][i];
      }
      Input_set(ctx->input, A_STEREO);
      pthread_mutex_unlock(&ctx->input->mutex);
      idx = chunk = 0;
    }
  }

  void* midi_buffer;
	jack_nframes_t N;
	jack_nframes_t counter;

	midi_buffer = jack_port_get_buffer (midi_port, nframes);
	assert (midi_buffer);

	N = jack_midi_get_event_count (midi_buffer);
	for (counter = 0; counter < N; ++counter) {
		jack_midi_event_t event;
		int r;
		r = jack_midi_event_get (&event, midi_buffer, counter);

		if (r != 0) { 
      continue;
    }

    if (event.size > MSG_BUFFER_SIZE) {
			fprintf(stderr, "Error: MIDI message was too large, skipping event. Max. allowed size: %d bytes\n", MSG_BUFFER_SIZE);
    } else if (jack_ringbuffer_write_space (rb) >= sizeof(midimsg)) {
		  midimsg m;
		  m.tme_mon = monotonic_cnt;
		  m.tme_rel = event.time;
		  m.size    = event.size;
		  memcpy (m.buffer, event.buffer, event.size);
		  jack_ringbuffer_write (rb, (void *) &m, sizeof(midimsg));
      execute_event (&m, ctx); 
      jack_ringbuffer_reset (rb);    
    } else {
			fprintf (stderr, "Error: ringbuffer was full, skipping event.\n");
    }
	}

  monotonic_cnt += nframes;
  if (pthread_mutex_trylock (&msg_thread_lock) == 0) {
		pthread_cond_signal (&data_ready);
		pthread_mutex_unlock (&msg_thread_lock);
  }
  return 0;
}


static void
setup_ports()
{
  int i;

  input_ports = xcalloc(2, sizeof(jack_port_t *));

  for (i = 0; i < 2; i++) {
    char name[64];

    sprintf(name, "input_%d", i);

    if ((input_ports[i] = jack_port_register(client, name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0)) == 0) {
      fprintf(stderr, "[!] JACK: cannot register input port \"%s\" !\n", name);
      jack_client_close(client);
      exit(1);
    } else {
      printf("[i] JACK: registered %s\n", name);
    }
  }

  ports = jack_get_ports(client, NULL, NULL, JackPortIsPhysical|JackPortIsOutput);
  if (NULL == ports) {
    xerror("JACK: no physical capture ports\n");
  }

  //Add a midi input port
  midi_port = jack_port_register (client, "input", JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
  if (midi_port == NULL) {
	  fprintf (stderr, "Could not register port.\n");
		exit (EXIT_FAILURE);
  }
}


void
create(Context_t *ctx)
{
  int i;
  char file[256];
  
  if ((client = jack_client_open(PACKAGE, JackNullOption, NULL)) == 0) {
    xerror("JACK server not running ?\n");
  }

  rb = jack_ringbuffer_create (RBSIZE * sizeof(midimsg));

  strcat(strcpy(file, getenv("HOME")), "/.lebiniou/midi_cc.xml");
  Midi_commands_load(file, midi_cc);

  jack_set_process_callback(client, process, ctx);
  //jack_set_process_callback(client, jack_midi_listen, ctx);
  jack_on_shutdown(client, jack_shutdown, ctx);

  if (NULL == (source_names[0] = getenv("LEBINIOU_JACK_LEFT"))) {
    source_names[0] = "system:capture_1";
  }
  if (NULL == (source_names[1] = getenv("LEBINIOU_JACK_RIGHT"))) {
    source_names[1] = "system:capture_2";
  }

  printf("[i] JACK: left  capture from %s\n", source_names[0]);
  printf("[i] JACK: right capture from %s\n", source_names[1]);

  setup_ports();

  jack_nframes_t jack_size = jack_get_buffer_size(client);
  printf("[i] JACK: buffer size: %d\n", jack_size);
  if (jack_size >= insize) {
    chunks = 1;
    insize = jack_size;
  } else {
    chunks = insize / jack_size;
  }
  printf("[i] JACK: %d chunks to read\n", chunks);

  ctx->input = Input_new(insize);
  data[0] = xcalloc(insize, sizeof(double));
  data[1] = xcalloc(insize, sizeof(double));

  if (jack_activate(client)) {
    xerror("JACK: cannot activate client\n");
  }

  for (i = 0; i < 2; i++) {
    if (jack_connect(client, ports[i], jack_port_name(input_ports[i]))) {
      fprintf(stderr, "[!] JACK: can not connect input port %s to %s\n", jack_port_name(input_ports[i]), source_names[i]);
      jack_client_close(client);
      exit(1);
    } else {
      printf("[i] JACK: connected %s to %s\n", source_names[i], jack_port_name(input_ports[i]));
    }
  }
  jack_free(ports);
}


void
destroy(Context_t *ctx)
{
  jack_client_close(client);
  Input_delete(ctx->input);
  xfree(data[0]);
  xfree(data[1]);
}
