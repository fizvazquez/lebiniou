/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"
#include "pbar.h"


PBar_t *
pbar_new()
{
  PBar_t *pb;

  pb = xcalloc(1, sizeof(PBar_t));

  return pb;
}


void
pbar_delete(PBar_t *pb)
{
  xfree(pb);
}


void
pbar_step(PBar_t *pb)
{
  if (pb != NULL) {
    static char progress[] = "/-\\|";

    printf("%c%c", progress[(int)pb->step++], 8);
    fflush(stdout);
    if (pb->step == 4) {
      pb->step = 0;
    }
  }
}
