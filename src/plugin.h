/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_PLUGIN_H
#define __BINIOU_PLUGIN_H

#include "options.h"

enum PluginType { PL_INPUT, PL_MAIN, PL_OUTPUT };

struct Context_s;

typedef struct Plugin_s {
  void   *handle;     /* .so handle */

  uint32_t id;
  u_long *options;
  u_long *mode;

  char   *name;
  char   *file;       /* to unload/reload */
  char   *dname;      /* display name */
  char   *desc;       /* plugin description */

  pthread_t thread;

  u_long calls;       /* number of times this plugin was run */

  /* TODO a struct callbacks */
  /* callbacks */
  void   (*create)(struct Context_s *);	        /* constructor */
  void   (*destroy)(struct Context_s *);	/* destructor */

  void   (*run)(struct Context_s *);	        /* run function */
  void * (*jthread)(void *);                    /* joinable thread */

  void   (*on_switch_on)(struct Context_s *);	/* switching on */
  void   (*on_switch_off)(struct Context_s *);	/* switching off */

  /* Output plugin stuff */
  void (*fullscreen)(const int);
  void (*switch_cursor)();
} Plugin_t;

Plugin_t *Plugin_new(const char *, const char *, const enum PluginType);
void Plugin_delete(Plugin_t *);
void Plugin_reload(Plugin_t *);

void Plugin_init(Plugin_t *);

char *Plugin_name(const Plugin_t *);
char *Plugin_dname(const Plugin_t *);

#endif /* __BINIOU_PLUGIN_H */
