BEGIN {

}


function pmod(mod) {
    if (mod == "-")
	return;
    if (mod == "A")
	return "Alt+";
    if (mod == "C")
	return "Ctrl+";
    if (mod == "S")
	return "Shift+";
    if (mod == "CS")
	return "Ctrl-Shift-";
}


function cmod(mod) {
    if (mod == "-")
	return "BKEY";
    if (mod == "A")
	return "BALT";
    if (mod == "C")
	return "BCTRL";
    if (mod == "S")
	return "BSHIFT";
}


{
    if (($1 == "#") || ($0 == "") || ($1 == "*") || ($1 == "-"))
	next;
  
    tail = substr($0, (length($1 $2 $3 $4 $5 $6) + 7));

    if (tail != "") {
	printf "[B<%s%s>] - %s\n", pmod($2), $3, tail;
	print "";
    } else {
	printf "[%s%s] - *** TODO document me ! ***\n", pmod($2), $3;
	print "";
    }
}


END {

}
