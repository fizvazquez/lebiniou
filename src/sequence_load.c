/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"
#include "xmlutils.h"
#include "images.h"
#include "colormaps.h"


/*
 * Left as an exercise to the reader: this code is not robust at all
 * we expect to read files that have been writen by Sequence_write.
 * But we are talibans and are not kind with wrong inputs.
 */
Sequence_t *
Sequence_load(const char *file)
{
  Sequence_t *s = NULL;
  xmlDocPtr doc = NULL; /* XmlTree */
  xmlNodePtr sequence_node = NULL, sequence_node_save = NULL, plugins_node = NULL;
  int res;
  long tmp;
  char *dot = NULL;
  gchar *blah = NULL;
  xmlChar *youhou;
  int legacy = 0;

  if (file == NULL) {
    xerror("Attempt to load a sequence with a NULL filename\n");
  }

  dot = strrchr(file, '.');
  if ((dot == NULL) || strcasecmp(dot, ".xml")) {
#ifdef DEBUG
    printf("[!] Not a sequence filename: '%s'\n", file);
#endif
    return NULL;
  }

#ifdef DEBUG
  printf("[i] Loading sequence from file '%s'\n", file);
#endif

  /* BLA ! */
  xmlKeepBlanksDefault(0);
  xmlSubstituteEntitiesDefault(1);

  /*
   * build an XML tree from the file
   */
  blah = g_strdup_printf("%s/%s", Sequences_get_dir(), file);

  doc = xmlParseFile(blah);
  g_free(blah);
  if (doc == NULL) {
    xerror("xmlParseFile error\n");
  }

  sequence_node = xmlDocGetRootElement(doc);
  if (sequence_node == NULL) {
    VERBOSE(printf("[!] Sequence %s: xmlDocGetRootElement error\n", file));
    goto error;
  }

  sequence_node = xmlFindElement("sequence", sequence_node);
  if (sequence_node == NULL) {
    VERBOSE(printf("[!] Sequence %s: no <sequence> found\n", file));
    goto error;
  }

  youhou = xmlGetProp(sequence_node, (const xmlChar *)"id");
  tmp = getintfield(youhou);
  xmlFree(youhou);

  if (tmp <= 0) {
    VERBOSE(printf("[!] Sequence %s: id must be > 0\n", file));
    goto error;
  }

  s = Sequence_new(tmp);

  /* first, get <auto_colormaps> */
  sequence_node_save = sequence_node = sequence_node->xmlChildrenNode;

  sequence_node = xmlFindElement("auto_colormaps", sequence_node);
  if (sequence_node == NULL) {
    sequence_node = sequence_node_save;
    goto bare_sequence;
  }
  res = xmlGetOptionalLong(doc, sequence_node, &tmp);
  if (res == -1) {
    s->auto_colormaps = 0;
  } else {
    s->auto_colormaps = (u_char)tmp;
  }
  assert((s->auto_colormaps == 0) || (s->auto_colormaps == 1));
#ifdef DEBUG
  printf("[i] Random colormaps: %s\n", (s->auto_colormaps ? "on" : "off"));
#endif

  if (!s->auto_colormaps) {
    /* not auto*, get colormap name */
    char *cmap = xmlGetMandatoryString(doc, "colormap", sequence_node);
#ifdef DEBUG
    printf("[i] Need colormap: '%s'\n", cmap);
#endif
    s->cmap_id = Colormaps_find(cmap);
    xfree(cmap);
  } else {
    s->cmap_id = Colormaps_random_id();
  }

  /* then, get  <auto_images> or (legacy) <auto_pictures> */
  sequence_node_save = sequence_node = sequence_node->next;
  sequence_node = xmlFindElement("auto_images", sequence_node);
  if (sequence_node == NULL) {
    sequence_node = sequence_node_save;
    sequence_node = xmlFindElement("auto_pictures", sequence_node);
    if (sequence_node == NULL) {
      VERBOSE(printf("[!] Sequence %s: no <auto_images> or <auto_pictures> found\n", file));
      goto error;
    } else {
      legacy = 1;
    }
  }
  res = xmlGetOptionalLong(doc, sequence_node, &tmp);
  if (res == -1) {
    s->auto_images = 0;
  } else {
    s->auto_images = (u_char)tmp;
  }
  assert((s->auto_images == 0) || (s->auto_images == 1));
#ifdef DEBUG
  printf("[i] Random images: %s\n", (s->auto_images ? "on" : "off"));
#endif

  if (!s->auto_images) {
    /* not auto*, get image name */
    char *image = xmlGetMandatoryString(doc, legacy ? "picture" : "image", sequence_node);
#ifdef DEBUG
    printf("[i] Need image: '%s'\n", image);
#endif
    if (images == NULL) {
      VERBOSE(printf("[!] No images are loaded, won't find '%s'\n", image));
      xfree(image);
      goto error;
    } else {
      s->image_id = Images_find(image);
      if (s->image_id == 0) {
        VERBOSE(printf("[!] Image '%s' not found, using default\n", image));
      }
      xfree(image);
    }
  } else {
    if (images == NULL) {
      s->broken = 1;
      s->image_id = -1;
      s->auto_images = 0;
    } else {
      s->image_id = Images_random_id();
    }
  }

bare_sequence:
  /* now, get plugins */
  plugins_node = xmlFindElement("plugins", sequence_node);
  if (plugins_node == NULL) {
    VERBOSE(printf("[!] Sequence %s: no <plugins> found\n", file));
    goto error;
  }
  plugins_node = plugins_node->xmlChildrenNode;
  if (plugins_node == NULL) {
    VERBOSE(printf("[!] Sequence %s: no elements in <plugins>\n", file));
    goto error;
  }

  while (plugins_node != NULL) {
    u_char lens;
    Layer_t *layer;
    Plugin_t *p;

    assert(plugins_node->name != NULL);
    lens = !xmlStrcmp(plugins_node->name, (const xmlChar *)"lens");

    youhou = xmlGetProp(plugins_node, (const xmlChar *)"id");
    tmp = getintfield(youhou);
    xmlFree(youhou);
    if (!tmp) {
      goto error;
    }

    p = Plugins_find((u_long)tmp);
    if (p == NULL) {
      goto error;
    }

    if (!(*p->options & BEQ_DISABLED)) {
      enum LayerMode mode;

      layer = Layer_new(p);

      youhou = xmlGetProp(plugins_node, (const xmlChar *)"mode");
      if (youhou != NULL) {
        mode = LayerMode_from_string((const char *)youhou);
        xmlFree(youhou);
      } else {
        mode = NORMAL;
      }
      layer->mode = mode;

      s->layers = g_list_append(s->layers, (gpointer)layer);

      if (lens) {
        s->lens = (Plugin_t *)p;
      }
    }

    plugins_node = plugins_node->next;
  }

  /* Check sequence length */
  assert(Sequence_size(s) <= MAX_SEQ_LEN);

  /* Clean up */
  xmlFreeDoc(doc);
  xmlCleanupParser(); /* FIXME is this ok ? */

  *dot = '\0'; /* spr0tch */
  s->name = strdup(file);

  return s;

error:
  VERBOSE(printf("[!] Failed to load sequence from file '%s'\n", file));

  /* Clean up */
  xmlFreeDoc(doc);
  xmlCleanupParser(); /* FIXME is this ok ? */

  Sequence_delete(s);

  return NULL;
}
