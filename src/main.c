/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.h"
#include "events.h"
#include "images.h"
#include "colormaps.h"


char *data_dir = NULL;
char *base_dir = NULL;
char *schemes_file = NULL;
char *input_plugin = NULL;
char *output_plugin = NULL;
char fullscreen = 0;
long max_fps = 50;
u_short width = DEFAULT_WIDTH;
u_short height = DEFAULT_HEIGHT;
enum RandomMode random_mode = BR_SCHEMES;
char *pid_file  = "/tmp/lebiniou.pid";
char *themes = NULL;
#ifdef WITH_WEBCAM
int webcams = 1;
int hflip = 1;
int vflip = 1;
char *video_base;
#else
int webcams = 0;
#endif
uint8_t statistics = 0; // Display statistics
char *video_filename = NULL; // video plugin

/* default input size (samples) */
#define DEFAULT_INPUT_SIZE 1024
uint32_t input_size = DEFAULT_INPUT_SIZE;
/* default delay for phase-space reconstructions */
#define DEFAULT_PHASE_SPACE_DELAY 10
uint8_t phase_space_delay = DEFAULT_PHASE_SPACE_DELAY;
/* default spline span size */
#define DEFAULT_SPAN_SIZE 6
uint8_t span_size = DEFAULT_SPAN_SIZE;
/* volume scaling */
double volume_scale = 1.0;


#define _BANNER "\n"					\
  "      _          ___ _      _          \n"		\
  "     | |   ___  | _ | )_ _ ( )___ _  _ \n"		\
  "     | |__/ -_) | _ \\ | ' \\| / _ \\ || |\n"	\
  "     |____\\___| |___/_|_||_|_\\___/\\_,_|\n"	\
  "\n"							\
  "           .:[ " PACKAGE_STRING " ]:.\n"             \
  " -------------------------------------------\n"	\
  "  \"Une cause très petite, qui nous échappe,\n"	\
  "  détermine un effet considérable que nous \n"	\
  "  ne pouvons pas ne pas voir, et alors nous\n"	\
  "  disons que cet effet est dû au hasard.\" \n"	\
  "\n"				 			\
  "    -- Henri Poincaré, 1908\n"  	                \
  " -------------------------------------------\n"


static inline void
do_banner(void)
{
  VERBOSE(printf(_BANNER));
}


static char *
get_pid_file()
{
  static char buff[1025];

  if (NULL == pid_file) {
    xerror("NULL pid_file");
  }

  memset(buff, '0', 1025*sizeof(char));
  snprintf(buff, 1024*sizeof(char), "%s", pid_file);

#ifdef DEBUG
  printf("[i] PID file: %s\n", buff);
#endif

  return buff;
}


static void
write_pid_file()
{
  int fd, res2;
  ssize_t res;
  char *buff;

  buff = get_pid_file();

  fd = open(buff, O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR);
  if (-1 == fd) {
    xperror("creat");
  }

  /* buff is 1025 char long */
  memset(buff, '0', 1025*sizeof(char));
  snprintf(buff, 1024*sizeof(char), "%d", getpid());
  res = write(fd, buff, strlen(buff)*sizeof(char));
  if (-1 == res) {
    xperror("write");
  }

  res2 = close(fd);
  if (-1 == res2) {
    xperror("close");
  }
}


static void
remove_pid_file()
{
  char *buff = get_pid_file();
  int res;

  res = unlink(buff);
  if (-1 == res) {
    xperror("unlink");
  }
}


int
main(int argc, char **argv)
{
  read_keyfile();
  getargs(argc, argv);
  register_signals();

  /* Banner */
  if (NULL == getenv("LEBINIOU_NO_BANNER")) {
    do_banner();
  }

  /* Initialize library */
  biniou_new(data_dir, base_dir, schemes_file, themes,
#ifndef FIXED
             width, height,
#endif
             B_INIT_ALL
#ifdef DEBUG
             |B_INIT_VERBOSE
#endif
             , input_size, phase_space_delay, span_size, webcams);
  xfree(themes);

  if (statistics) {
    biniou_start();
    if (NULL != plugins) {
      printf("[i] Plugins: %d\n", plugins->size);
    } else { // Should not happen, added just in case
      printf("[i] No plugins\n");
    }
    if (NULL != colormaps) {
      printf("[i] Colormaps: %d\n", colormaps->size);
    } else { // Should not happen, added just in case
      printf("[i] No colormaps\n");
    }
    if (NULL != images) {
      printf("[i] Images: %d\n", images->size);
    } else { // Should not happen, added just in case
      printf("[i] No images\n");
    }
    libbiniou_verbose = 0;
    biniou_go(1, 1);
    biniou_delete();
    exit(0);
  }

  write_pid_file();

  biniou_set_max_fps(max_fps);
  biniou_set_random_mode(random_mode);

  /* Load input */
  if (NULL != input_plugin) {
    biniou_load_input(base_dir, input_plugin, volume_scale);
    xfree(input_plugin);
  }

  /* Load output */
  if (NULL != output_plugin) {
    biniou_load_output(base_dir, output_plugin);
    xfree(output_plugin);
    biniou_set_full_screen(fullscreen);
  }

  /* Main loop */
  biniou_run();

  /* All done */
  biniou_delete();

  remove_pid_file();

  okdone("Quit: ouh-ouuuuh \\o/");

  return 0;
}
