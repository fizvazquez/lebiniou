/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <inttypes.h>
#include "images.h"
#include "brandom.h"
#include "pbar.h"


Images_t *images = NULL;


static int
Images_compare(const void *_a, const void *_b)
{
  Image8_t **a = (Image8_t **)_a;
  Image8_t **b = (Image8_t **)_b;

  assert(*a != NULL);
  assert(*b != NULL);
  assert((*a)->dname != NULL);
  assert((*b)->dname != NULL);

  return strcasecmp((*a)->dname, (*b)->dname);
}


void
Images_new(const char *basedir, const char *themes)
{
  DIR *dir;
  struct dirent *entry;
  GSList *tmp = NULL;
  uint16_t size = 0;
  GSList *t;
  PBar_t *pb = NULL;
  gchar **tokens, **theme;

  assert(basedir != NULL);
  assert(themes != NULL);

  if (libbiniou_verbose) {
    pb = pbar_new();
  }

  tokens = g_strsplit(themes, ",", 0);
  theme = tokens;

  for ( ; *theme != NULL; theme++) {
    char *directoryname;
    char *th = *theme;

    VERBOSE(printf("[+] Loading theme '%s': ", th));
    fflush(stdout);
    if (*th == '~') {
      th++;
      if (*th != '\0') {
        directoryname = g_strdup_printf("%s/." PACKAGE_NAME "/images/%s", g_get_home_dir(), th);
      } else {
        fprintf(stderr, "[!] Not a valid tilde-theme: %s\n", *theme);
        continue;
      }
    } else {
      directoryname = g_strdup_printf("%s/%s", basedir, *theme);
    }

    dir = opendir(directoryname);
    if (dir == NULL) {
      fprintf(stderr, "[!] Error while reading image directory %s content: %s\n",
              directoryname, strerror(errno));
      g_free(directoryname);
      continue;
    }

    while ((entry = readdir(dir)) != NULL) {
      uint32_t hash;
      Image8_t *pic;

      if (entry->d_name[0] == '.') {
        continue;
      }

      hash = FNV_hash(entry->d_name);

      char ignore = 0;
      for (t = g_slist_next(tmp);
           t != NULL;
           t = g_slist_next(t))
        if (((Image8_t *)t->data)->id == hash) {
          const char *name = ((Image8_t *)t->data)->name;

          if (strcmp(name, entry->d_name))
            printf("[!] Ignoring image '%s'\n"
                   "[!]   (same hash as '%s': %"PRIu32")\n",
                   entry->d_name, name, hash);
          ignore = 1;
        }

      if (!ignore) {
        pic = Image8_new();

        if (Image8_load(pic, hash, directoryname,
                        entry->d_name) != 0) {
          Image8_delete(pic);
        } else {
          size++;
          if (libbiniou_verbose && !(size % 5)) {
            pbar_step(pb);
          }

          tmp = g_slist_prepend(tmp, (gpointer)pic);
        }
      }
    }

    if (closedir(dir) == -1) {
      xperror("closedir");
    }
    g_free(directoryname);
    VERBOSE(printf("done.\n"));
  }

  g_strfreev(tokens);

  images = xmalloc(sizeof(Images_t));
  if (libbiniou_verbose) {
    pbar_delete(pb);
    VERBOSE(printf("[p] Loaded %d images\n", size));
  }

  if (size) {
    uint16_t i;

    images->imgs = xmalloc(size * sizeof(Image8_t *));
    for (i = 0, t = tmp;
         t != NULL;
         t = g_slist_next(t), i++) {
      images->imgs[i] = (Image8_t *)t->data;
    }
    g_slist_free(tmp);
    images->size = size;

    qsort((void *)images->imgs, (size_t)images->size,
          (size_t)sizeof(Image8_t *), &Images_compare);
  } else {
    xfree(images);
  }
}


void
Images_delete()
{
  if (images != NULL) {
    uint16_t i;

    for (i = 0; i < images->size; i++) {
      Image8_delete(images->imgs[i]);
    }
    xfree(images->imgs);
    xfree(images);
  }
}


const char *
Images_name(const uint32_t id)
{
  uint16_t i;

  if (images == NULL) {
    VERBOSE(fprintf(stderr, "[!] No images loaded\r\n"));
    return NULL;
  }

  for (i = 0; i < images->size; i++)
    if (images->imgs[i]->id == id) {
      return images->imgs[i]->name;
    }

  if (id == 0) {
    return images->imgs[0]->name;
  }

  VERBOSE(fprintf(stderr, "[!] Images_name: id %"PRIu16" not found\r\n", id));

  return NULL;
}


int32_t
Images_index(const uint32_t id)
{
  uint16_t i;

  if (images == NULL) {
    fprintf(stderr, "[!] No images loaded\r\n");
    return -1;
  }

  for (i = 0; i < images->size; i++)
    if (images->imgs[i]->id == id) {
      return i;
    }

  VERBOSE(fprintf(stderr, "[!] Images_index: id %"PRIu16" not found\r\n", id));

  return -1;
}


uint32_t
Images_find(const char *name)
{
  uint16_t i;

  if (images == NULL) {
    fprintf(stderr, "[!] No images loaded\r\n");
    return 0;
  }

  for (i = 0; i < images->size; i++)
    if (!strcmp(images->imgs[i]->name, name)) {
      return images->imgs[i]->id;
    }

  VERBOSE(fprintf(stderr, "[!] Image '%s' not found\r\n", name));

  return images->imgs[0]->id; /* Use the first image by default */
}


uint32_t
Images_random_id()
{
  uint16_t idx = 0;

  assert(images != NULL);
  if (!images->size) {
    return 0;
  }

  if (images->size > 1) {
    idx = b_rand_int_range(0, images->size - 1);
  }

  return images->imgs[idx]->id;
}


const Image8_t *
Images_random()
{
  uint16_t idx = 0;

  assert(images != NULL);
  if (NULL == images) {
    return NULL;
  }

  if (images->size > 1) {
    idx = b_rand_int_range(0, images->size - 1);
  }

  return images->imgs[idx];
}
