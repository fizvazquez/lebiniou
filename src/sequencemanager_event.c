/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"
#include "sequencemanager.h"


int
SequenceManager_event(SequenceManager_t *sm, const Event_t *e,
                      const char auto_colormaps, const char auto_images)
{
  switch (e->cmd) {
  case BC_SWITCH:
    if (e->arg0 == BA_LENS) {
      SequenceManager_toggle_lens(sm->cur);
      return 1;
    }
    break;

  case BC_MOVE:
    if (e->arg0 == BA_UP) {
      SequenceManager_move_selected_front(sm->cur);
      return 1;
    } else if (e->arg0 == BA_DOWN) {
      SequenceManager_move_selected_back(sm->cur);
      return 1;
    } else {
      return 0;
    }
    break;

  case BC_PREV:
    if (e->arg0 == BA_LAYER_MODE) {
      SequenceManager_prev_layer_mode(sm->cur);
    } else {
      SequenceManager_select_previous_plugin(sm->cur);
    }
    return 1;
    break;

  case BC_NEXT:
    if (e->arg0 == BA_LAYER_MODE) {
      SequenceManager_next_layer_mode(sm->cur);
    } else {
      SequenceManager_select_next_plugin(sm->cur);
    }
    return 1;
    break;

  case BC_RESET:
    if (e->arg0 == BA_LAYER_MODE) {
      SequenceManager_default_layer_mode(sm->cur);
      return 1;
    } else if (e->arg0 == BA_SEQUENCE) {
      Sequence_clear(sm->cur, 0);
      return 1;
    } else {
      return 0;
    }
    break;

  case BC_SAVE:
    if (e->arg0 == BA_SEQUENCE_FULL) {
#ifdef DEBUG
      printf("[i] Save full sequence\n");
#endif
      Sequence_save(sm->cur, 0, SequenceManager_is_transient(sm, sm->cur),
                    TRUE, auto_colormaps, auto_images);
      sm->curseq = sequences->seqs;
      Shuffler_grow_one_left(sequences->shuffler);
      return 1;
    } else if (e->arg0 == BA_OVERWRITE_FULL) {
#ifdef DEBUG
      printf("[i] Update full sequence\n");
#endif
      Sequence_save(sm->cur, 1, SequenceManager_is_transient(sm, sm->cur),
                    TRUE, auto_colormaps, auto_images);
      return 1;
    } else if (e->arg0 == BA_SEQUENCE_BARE) {
#ifdef DEBUG
      printf("[i] Save bare sequence\n");
#endif
      Sequence_save(sm->cur, 0, SequenceManager_is_transient(sm, sm->cur),
                    FALSE, auto_colormaps, auto_images);
      sm->curseq = sequences->seqs;
      Shuffler_grow_one_left(sequences->shuffler);
      return 1;
    } else if (e->arg0 == BA_OVERWRITE_BARE) {
#ifdef DEBUG
      printf("[i] Update bare sequence\n");
#endif
      Sequence_save(sm->cur, 1, SequenceManager_is_transient(sm, sm->cur),
                    FALSE, auto_colormaps, auto_images);
      return 1;
    }
    break;

  default:
    break;
  }

  return 0;
}
