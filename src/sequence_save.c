/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <inttypes.h>
#include "globals.h"
#include "xmlutils.h"
#include "sequence.h"
#include "images.h"
#include "colormaps.h"


static void
Sequence_write(const Sequence_t *s, const char *filename,
               const uint8_t full)
{
  xmlDoc *doc;
  GList *layers;
  xmlNode *node, *plugins_node;

  /* FIXME check return code of xml* functions */
  doc = xmlNewDoc((const xmlChar *)"1.0");
  node = doc->children = xmlNewDocNode(doc, NULL, (const xmlChar *)"sequence", NULL);
  xml_set_id(node, s->id);

  if (full) {
    /* here auto_colormaps and auto_images are 0 or 1, not -1 */
    xmlNewChild(node, NULL, (const xmlChar *)"auto_colormaps",
                (const xmlChar *)((s->auto_colormaps) ? "1" : "0"));
    if (!s->auto_colormaps) {
      xmlNewTextChild(node, NULL, (const xmlChar *)"colormap", (const xmlChar *)Colormaps_name(s->cmap_id));
    }

    xmlNewChild(node, NULL, (const xmlChar *)"auto_images",
                (const xmlChar *)((s->auto_images) ? "1" : "0"));
    if (!s->auto_images) {
      xmlNewTextChild(node, NULL, (const xmlChar *)"image", (const xmlChar *)Images_name(s->image_id));
    }
  }

  /* create <plugins> block */
  plugins_node = xmlNewChild(node, NULL, (const xmlChar *)"plugins", NULL);

  /* iterate over plugins list */
  for (layers = g_list_first(s->layers); layers != NULL; layers = g_list_next(layers)) {
    Layer_t *layer = (Layer_t *)layers->data;
    Plugin_t *p = layer->plugin;
    xmlNode *nd;
    const char *tmp;

    /* decide whether block is <lens> or <plugin> */
    if ((s->lens != NULL) && (p == s->lens)) {
      tmp = "lens";
    } else {
      tmp = "plugin";
    }

    nd = xmlNewChild(plugins_node, NULL, (const xmlChar *)tmp, NULL);

    /* we store the name, but only for informational purposes */
    xmlSetProp(nd, (const xmlChar *)"name", (const xmlChar *)p->name);

    /* store id */
    xml_set_id(nd, p->id);

    /* store layer mode */
    tmp = LayerMode_to_string(layer->mode);
    xmlSetProp(nd, (const xmlChar *)"mode", (const xmlChar *)tmp);
  }

  xmlKeepBlanksDefault(0);
  xmlSaveFormatFile(filename, doc, 1);
  xmlFreeDoc(doc);
}


void
Sequence_save(Sequence_t *s, int overwrite, const int is_transient,
              const uint8_t bare, const char auto_colormaps, const char auto_images)
{
  char *filename = NULL;
  Sequence_t *store = NULL;

  if (g_list_length(s->layers) == 0) {
    printf("[!] *NOT* saving an empty sequence !\n");
    return;
  }

  if (s->broken) {
    printf("[!] Sequence is broken, won't save !\n");
    return;
  }

  if (overwrite && (s->id == 0)) {
    printf("[!] Overwriting a NEW sequence == saving\n");
    overwrite = 0;
  }

  if (!overwrite || is_transient) {
    s->id = unix_timestamp();
  }

  if (s->name != NULL) {
    xfree(s->name);
  }
  s->name = g_strdup_printf("%"PRIu32, s->id);
  printf("[s] Saving sequence %"PRIu32"\n", s->id);

  const gchar *blah = Sequences_get_dir();
  rmkdir(blah);
  // g_free(blah);

  if (overwrite) {
    filename = g_strdup_printf("%s/%s.xml", blah, s->name);
  } else {
    filename = g_strdup_printf("%s/%"PRIu32".xml", blah, s->id);
  }

  printf("[s] Filename: %s\n", filename);

  /* set auto_colormaps from context if needed */
  if (s->auto_colormaps == -1) {
    s->auto_colormaps = auto_colormaps;
  }
  /* set auto_images from context if needed */
  if (s->auto_images == -1) {
    s->auto_images = auto_images;
  }

  Sequence_write(s, filename, bare);

  g_free(filename);

  s->changed = 0;

  if (overwrite) {
    GList *oldp = g_list_find_custom(sequences->seqs, (gpointer)s, Sequence_sort_func);
    Sequence_t *old;

    if (oldp != NULL) {
      old = (Sequence_t *)oldp->data;
      Sequence_copy(s, old);
    } else {
      overwrite = 0;
    }
  }

  if (overwrite == 0) {
    /* new sequence */
    store = Sequence_new(0);
    Sequence_copy(s, store);

    sequences->seqs = g_list_prepend(sequences->seqs, (gpointer)store);
    sequences->size++;
  }
}
