/*
 *  Copyright 1994-2019 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_PLUGINS_H
#define __BINIOU_PLUGINS_H

#include "plugin.h"
#include "event.h"


typedef struct Plugins_s {
  char      *path;
  Plugin_t **plugins;
  short      size;
  short      selected_idx;
  Plugin_t  *selected;
} Plugins_t;


Plugins_t *Plugins_new(const char *);
void Plugins_delete(Plugins_t *);

void Plugins_load(Plugins_t *);
void Plugins_init(Plugins_t *);
void Plugins_reload_selected(Plugins_t *);

Plugin_t *Plugins_find(const u_long);
Plugin_t *Plugins_get_random(const enum PluginOptions);

void Plugins_select(Plugins_t *, const Plugin_t *);

void Plugins_next();
void Plugins_prev();
void Plugins_next_n(const u_short);
void Plugins_prev_n(const u_short);

int Plugins_event(Plugins_t *, const Event_t *);

/* Use with extreme care, this is not for beginners */
void Plugins_change_run_callback(const u_long, void (*)(struct Context_s *));

void Plugins_set_blacklist(gchar **);

#endif /* __BINIOU_PLUGINS_H */
