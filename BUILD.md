# Build options

The `configure` script will build all plugins if possible. If needed, you
can disable them using the corresponding `--disable-` option.

## Input plugins

* `--enable-alsa`: build the ALSA input plugin [default=yes]
* `--enable-jackaudio`: build the JACK Audio input plugin [default=yes]
* `--enable-pulseaudio`: build the PulseAudio input plugin [default=yes]
* `--enable-esd`: build the ESD input plugin [default=no]
* `--enable-sndfile`: build the SndFile input plugin [default=yes]
* `--enable-twip`: build the Twip input plugin [default=yes]

## Output plugins

* `--enable-caca`: build the libcaca plugin [default=yes]
* `--enable-diskwriter`: build the diskwriter output plugin [default=yes]

## Optional plugins

* `--enable-test-plugins`: build the test/debug plugins [default=no]
* `--enable-old-delay`: build old delay plugins [default=no]

# Engine options

* `--enable-fixed="WIDTHxHEIGHT"`: use fixed-size video buffers [default=no]
* `--enable-webcam`: enable webcam support [default=yes]
* `--enable-camsize="WIDTHxHEIGHT"`: set webcam capture size [default="640x480"]

# Debugging options

* `--enable-debug`: turn on debugging [default=no]
* `--enable-xdebug`: turn on extra debugging [default=no]
* `--enable-warnings`: treat warnings as errors [default=yes]
* `--enable-dlclose`: call dlclose() when unloading plugins [default=yes]

# Other options

* `--enable-opengl`: enable OpenGL support [default=no]
