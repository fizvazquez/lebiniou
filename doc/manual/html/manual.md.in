<style>
  .note {
  color: red;
  }

  body {
  background-color: black;
  color: white;
  }

  strong {
  background-color: #333;
  color: yellow;
  }

  code {
  font-size: 16px;
  }
  <!--
  .shell {
  background-color: black;
  color: lime;
  padding: 10px;
  border: solid white;
  }
  -->
</style>

# Le Biniou - User's manual

## Basic concepts

Le Biniou is made of a set of plugins (nearly one hundred), that produce
various graphic effects, depending on a source input (usually, your soundcard).

There are several types of plugins:
* Input plugins (sound inputs: OSS, ALSA, PulseAudio, ...)
* Drawing plugins (oscilloscopes, screensaver effects, ...)
* Filtering plugins (blurs, distortions, ...)
* Image plugins (that use a background image)
* Color plugins (modify the current colormap)

These plugins can be selected and organized in a particular order, forming "sequences".

Sequences can be created by the user (in the interactive mode), or by
Le Biniou's Artificial Intelligence (in the automatic mode).

Le Biniou has an integrated On Screen Display (OSD), that shows
you what's going on, and acts as a User Interface to select, organize or reorder
plugins when you're in the interactive mode.

By default, Le Biniou starts in automatic mode, without OSD.

Le Biniou can randomly pick between many different colormaps and background
images, allowing evolving rendering effects.

There are also three "auto-modes", that will randomly change the
current sequence between user-defined and/or Le Biniou-generated ones.

Keyboard controls will be shown like this: "Hit the __C25__ key".

<b class="note">Note:</b> This is just a quickstart guide, for a full list of keyboard controls, please refer to the man page:
```sh
$ man lebiniou
```

## Starting Le Biniou

Le Biniou can be started by:
* the Applications/Multimedia menu
* using the command line

Using the command line, you can set specific options, like
the display mode (window size, full-screen, etc.), the input source to use,
which On Screen Display mode to activate, and more.

Please refer to the <a href="#cl">"Command line options"</a> section for more informations.

Options can also be set with a user-defined <a href="#cf">configuration file</a>.

<b class="note">Note:</b> You can create a shortlink to your <b>~/.lebiniou</b> folder, so it is easily accessible:
```sh
$ ln -s ~/.lebiniou ~/Desktop/LeBiniou
```

## Essential keyboard controls

* Switch full-screen on/off using the __C5__ key
* Exit Le Biniou with __C9__
* Exit Le Biniou, saving the current sequence with __C10__. This sequence will be activated
next time you start Le Biniou.<br /><b class="note">Note:</b> Sequences are saved in your
  "<b>~/.lebiniou/sequences/</b>" folder.
* Take a screenshot with __C17__.<br /><b class="note">Note:</b> Screenshots are saved in your "<b>~/.lebiniou/screenshots/</b>" folder.
* Switch the webcam on/off using __C25__, set the reference image using __C26__
* Use __C29__ to use the next available webcam

## Automatic Modes
When started, Le Biniou will create a new sequence regularly using it's Artificial Intelligence
engine. Stop this mode by hitting __C13__.

If the OSD is activated, a progress bar on the right will show you the time remaining
until Le Biniou creates a new sequence.

* Use __C18__ to make the AI create a new sequence.
* Use __C20__ or __C21__ to switch between the different random modes:
  * Off
  * Select random user sequences
  * Select random AI sequences
  * Select random user and AI sequences
* Hit __C30__ to "lock" a plugin. When generating random sequences, Le Biniou will do it's best to include that plugin in new sequences. Use __C30__ again on the plugin to unlock it.

## Interactive Mode
At some point, you may want to create your own sequences. We will show you how to
do this with a complete example.

To create new sequences, it is recommended to set the OSD in full mode (so that you can see the sequence, the plugin list and their description), and of course to turn Automatic Mode off (using __C13__).

### Essential keyboard controls

#### Colormaps
* Use __CO1__/__CO2__ to cycle through the colormaps
* Use __CO3__ to randomly pick one
* Use __C22__ to switch "random colormaps mode" on/off.<br />In this mode, Le Biniou will change colormaps every few seconds
* Show or hide the current colormap using __C4__

#### Images
* Use __IM1__/__IM2__ to cycle through the images
* Use __IM3__ to randomly pick one
* Use __C23__ to switch "random images mode" on/off.<br />In this mode, Le Biniou will change images every few seconds

### Your first sequence
To start from the begining, use __S1__ to remove all the plugins, and __C12__ to clear the screen.

<b class="note">Note:</b> In the top-left part of the OSD, the sequence will have the name "(none)".

1. First, add a plugin: find the "rotors" plugin in the list and add it to the sequence.
2. Use __P1__/__P2__ to navigate through the list, and __C14__ to add or remove the plugin.
3. Use __P3__/__P4__ to scroll faster in the list.
4. Now, add a filtering effect: find the "blur2" plugin and add it.
New plugins will be added at the end of the sequence (except a few plugins, like the "clear" plugin, that will be added at the front).
5. Then, change the colormap (using the controls described before) to one that pleases you.
6. Finally, add a plugin that reacts to the sound, for example, the "X oscillo stereo" plugin.

You can save your newly created sequence using __S10__ (will include colormaps and images information), or __S12__ (will only save the plugins list).
From now on, the sequence will have a name (the timestamp you saved it), for example: "1300821622".
It will be saved in your "<b>~/.lebiniou/sequences/</b>" folder under the name "1300821622.xml".
Later, you can rename this file to a suitable name, e.g. "My&nbsp;cool&nbsp;sequence.xml". This new name will be displayed in the OSD.

You can update an existing sequence using __S11__ (full sequence), or __S13__ (only the plugins).

When you have several sequences, you can cycle through them using __C16__ and __C15__,
pick one at random using __C19__, and use them in the Automatic Mode. Use __C24__ to select
the most recent sequence.

#### Reorganizing the sequence
You can change the plugins order in the sequence:
* Use __S3__/__S4__ to select a plugin in the sequence (the selected plugin will have an arrow before it's name)
* Use __S5__/__S6__ to move the selected plugin up/down in the sequence

#### Example sequences
Example sequences are available, install them by issuing the following command in your home directory:
```sh
$ tar xvfz /usr/share/doc/lebiniou-data/examples/sequences.tar.gz
```

## Advanced topics

### Lens mode
Lens mode basically prevents video feedback - these plugins end the sequence, all effects
occuring after a lens won't be reinjected in the next run.

The lens plugins are:
* cth_[xy]roller
* edge
* emboss
* hodge
* kaleid2
* mosaic
* nspiral
* ripple
* taquin
* tunnel
* warp
* [xy]gum
* [xy]shaker
* [xy]wave
* zbroken
* zmonitor
* zreflector

Some plugins (e.g.: cth_[xy]roller, taquin) give nice results with lens mode off.

You can switch the "lens mode" on/off on any plugin with __S2__.

### Layer modes

You can control how a plugin effects are melt with the previous one by controlling it's "layer mode" (think of merging layers in the GIMP):

There are 6 layer modes available:

* NOR (Normal): plugin works with defaults
* OVL (Overlay): pixels are added to the previous layer, if not null
* XOR (XOR): pixels are xored with the previous layer
* AVG (Average): takes the average of the layers
* RND (Random): takes pixels randomly between the layers (~50% of each will be chosen)
* --- (NULL): does nothing. pretty useless unless you want to disable a plugin without removing it from the sequence

Select the next layer mode using __S9__ or reset to it's default using __S7__

### <a name="cf">Configuration file</a>
You can permanently set options in a configuration file (<b>~/.lebiniourc</b>), install this file by issuing the following command:
```sh
$ cp /usr/share/doc/lebiniou/examples/lebiniourc ~/.lebiniourc
```

This file is self-documented, and comes with suitable default values.

### Using your own images

Images are organized as "themes", that is, by directory.

Put your images under <b>~/.lebiniou/images/&lt;my_theme_name&gt;</b>.

Then start, using the "-t" switch with your theme name, prefixed with a "~":
```sh
$ lebiniou -t ~my_theme_name
```
Or a comma-separated list of themes:
```sh
$ lebiniou -t ~theme1,~theme2
```
Use the images from lebiniou-data package by removing the leading "~":
```sh
$ lebiniou -t biniou,~theme1,~theme2
```

You can also set this in your <b>~/.lebiniourc</b> configuration file. For example:
```
[Engine]
Themes = biniou,~theme1,~theme2
```

### Banks
You can bind sequences, colormaps and images to the function keys to organize and quickly activate them.
You are given 12 "banksets", each bankset contains 12 "banks" that can be mapped to sequences/colormaps/images.
* Select a bank mode:
  * Sequences: __BM1__
  * Colormaps: __BM2__
  * Images: __BM3__
    
* Select a bankset using __B1__..__B12__
  * Store the current sequence/colormap/image in a bank using __SB1__..__SB12__
  * Recall a stored sequence/colormap/image from a bank using __UB1__..__UB12__
  * Clear a bank using __CB1__..__CB12__

<b class="note">Note:</b> Banks are not saved to disk until you use __SB__.<br />
<b class="note">Note:</b> They are stored in your "<b>~/.lebiniou/banks.xml</b>" file.

### <a name="cl">Command line options</a>
* Display the list of available input/output plugins with:
```sh
$ lebiniou -h
```
* Use "-i &lt;INPUT&gt;" to select the input plugin.
* Use "-o &lt;OUTPUT&gt;" to select the output plugin.

### Environment variables
Some options can be configured through environment variables:

* Main application
  * LEBINIOU_NO_BANNER: do not display the banner on startup
  * LEBINIOU_WEBCAM: comma-separated list of options to pass to the webcam input:
    * webcams: number of webcams to use (default: 1)
    * device: path to the first webcam device (default: /dev/video)
    * hflip: flip webcam horizontally
    * vflip: flip webcam vertically
  * LEBINIOU_SEED: set this to a (positive) number to initialize the random number generator.

Example:
```sh
$ LEBINIOU_WEBCAM=webcams:1,device:/dev/video,hflip lebiniou
```

* Video input plugin
  * LEBINIOU_VIDEO: absolute path to a video to be played in loop. This can also be set in the
configuration file.

* SDL2/OSD font
  * LEBINIOU_FONT: absolute path to a .ttf font file
  * LEBINIOU_FONT_SIZE: the font size

* JACKAudio input plugin
  * LEBINIOU_JACK_LEFT
  * LEBINIOU_JACK_RIGHT

* <a href="http://www.mega-nerd.com/libsndfile/">sndfile</a> input plugin
  * LEBINIOU_SNDFILE: set the sample file to read
  * LEBINIOU_SNDFILE_LOOP: loop the sample

Example:
```sh
$ LEBINIOU_SNDFILE=/usr/share/sounds/alsa/Noise.wav LEBINIOU_SNDFILE_LOOP=true lebiniou -i sndfile
```

* RTMP output plugin (experimental feature)
  * LEBINIOU_RTMP_FFMPEG_ARGS
  * LEBINIOU_RTMP_URL

* libcaca output plugin
  * LEBINIOU_CACA_EXPORT_FORMAT
  * LEBINIOU_CACA_EXPORT_PREFIX

### 3D plugins
Some plugins (e.g.: the "galaxy" plugin) operate in 3D.
* Hit __C3__ to toggle 3D rotations on/off, __C1__ to randomize them
* Use __C2__ to display a bounding shape (cube, sphere, wired sphere, or none)
* Drag and drop the mouse to rotate the 3D view
* Use the mouse wheel to zoom in/out
</p>

<!--
### When compiled with OpenGL support
<p>
  <ul>
    * __C27__: Switch 3D cube on/off
    * __C28__: Toggle 3D world pulse on/off
  </ul>
</p>
-->

### Making videos from your sessions

If you need sound, the easiest way to record is to use some a screen-recorder (e.g. SimpleScreenRecorder, vokoscreen).

Nevertheless, there are two output plugins you can use:

1. mp4 plugin. This plugin will create a video in your "<b>~/.lebiniou/videos/</b>" folder.

Example: Assuming you have your track available in a format readable by libsndfile:
```sh
$ export LEBINIOU_SNDFILE=/path/to/track.wav
$ lebiniou -i sndfile -o SDL2,mp4
```

2. diskwriter plugin (legacy). This plugin dumps all frames to your "<b>~/.lebiniou/screenshots/</b>" folder, so that you can reassemble them to a video using e.g. ffmpeg.
<b class="note">Note:</b> Dumping to disk may slow down your system. To avoid this, put the screenshots folder on a SSD drive, or a RAM-disk. Or better, use the more recent
mp4 output plugin.

Example:
```sh
$ lebiniou -o SDL2,diskwriter
```

### Miscellaneous keyboard controls
* __C7__, __C8__: adjust the phase-space delay
* __C31__, __C32__: adjust the spline span size
* __C34__, __C35__: scale volume up/down
* __C33__: freeze/unfreeze sound
* __C6__: show/hide the mouse cursor
* __C11__: fill screen with random pixels
